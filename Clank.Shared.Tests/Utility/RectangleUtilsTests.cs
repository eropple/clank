﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Utility;
using Microsoft.Xna.Framework;
using NUnit.Core;
using NUnit.Framework;

namespace Clank.Tests.Utility
{
    [TestFixture]
    public class RectangleUtilsTests
    {

        [Test]
        public void CalculateBoundingTranslationContained()
        {
            var bounder = new Rectangle(10, 10, 200, 200);

            var bounded1 = new Rectangle(10, 10, 20, 20);
            Assert.AreEqual(Vector2.Zero, bounder.CalculateBoundingTranslation(bounded1));

            var bounded2 = new Rectangle(180, 180, 20, 20);
            Assert.AreEqual(Vector2.Zero, bounder.CalculateBoundingTranslation(bounded2));

            var bounded3 = new Rectangle(50, 50, 100, 100);
            Assert.AreEqual(Vector2.Zero, bounder.CalculateBoundingTranslation(bounded3));
        }

        [Test]

        public void CalculateBoundingTranslationNotContained()
        {
            {
                var bounder = new Rectangle(10, 10, 200, 200);

                var bounded1 = new Rectangle(8, 8, 20, 20);
                Assert.AreEqual(new Vector2(2, 2), bounder.CalculateBoundingTranslation(bounded1),
                    "top-left translation failed.");

                var bounded2 = new Rectangle(192, 192, 20, 20);
                Assert.AreEqual(new Vector2(-2, -2), bounder.CalculateBoundingTranslation(bounded2),
                    "bottom-right translation failed.");
            }
        }
    }
}
