﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clank.Utils
{
    public static class CollectionExtensions
    {
        public static TValue GetOrInsert<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey k, TValue v)
        {
            TValue ret;
            if (dict.TryGetValue(k, out ret)) return ret;

            ret = v;
            dict.Add(k, ret);
            return ret;
        }

        public static TValue GetOrElse<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey k, TValue v)
        {
            TValue ret;
            if (dict.TryGetValue(k, out ret)) return ret;

            ret = v;
            return ret;
        }

        public static TValue GetOrElse<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dict, TKey k, TValue v)
        {
            TValue ret;
            if (dict.TryGetValue(k, out ret)) return ret;

            ret = v;
            return ret;
        }

        public static IReadOnlyDictionary<TKey, TValue> Clone<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dict)
        {
            return dict.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public static IReadOnlyList<TValue> Clone<TValue>(this IReadOnlyList<TValue> list)
        {
            return list.Select(v => v).ToList();
        }
    }
}
