﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clank.Utils
{
    public static class Merging
    {
        /// <summary>
        /// Merges a set of dictionaries to come up with a single one. Later dictionaries override.
        /// </summary>
        public static Dictionary<TKey, TValue>
            MergeDictionaries<TKey, TValue>(IEnumerable<IDictionary<TKey, TValue>> dicts)
        {
            var retval = new Dictionary<TKey, TValue>();

            foreach (var dict in dicts.Reverse())
            {
                foreach (var kvp in dict)
                {
                    retval[kvp.Key] = kvp.Value;
                }
            }

            return retval;
        }

        public static Dictionary<TKey, TValue>
            MergeDictionaries<TKey, TValue>(IEnumerable<IReadOnlyDictionary<TKey, TValue>> dicts)
        {
            var retval = new Dictionary<TKey, TValue>();

            foreach (var dict in dicts.Reverse())
            {
                foreach (var kvp in dict)
                {
                    retval[kvp.Key] = kvp.Value;
                }
            }

            return retval;
        }

        public static Dictionary<TKey, TValue>
            MergeDictionaries<TKey, TValue>(params IDictionary<TKey, TValue>[] dicts)
        {
            var retval = new Dictionary<TKey, TValue>();

            foreach (var dict in dicts.Reverse())
            {
                foreach (var kvp in dict)
                {
                    retval[kvp.Key] = kvp.Value;
                }
            }

            return retval;
        }

        public static Dictionary<TKey, TValue>
            MergeDictionaries<TKey, TValue>(params IReadOnlyDictionary<TKey, TValue>[] dicts)
        {
            var retval = new Dictionary<TKey, TValue>();

            foreach (var dict in dicts.Reverse())
            {
                foreach (var kvp in dict)
                {
                    retval[kvp.Key] = kvp.Value;
                }
            }

            return retval;
        }
    }
}
