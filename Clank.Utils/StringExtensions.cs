﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clank.Utils
{
    public static class StringExtensions
    {
        public static Boolean ToBoolean(this String str)
        {
            str = str.ToLowerInvariant();
            switch (str)
            {
                case "yes":
                case "1":
                case "true":
                case "on":
                    return true;
                case "no":
                case "0":
                case "false":
                case "off":
                    return false;
                default:
                    throw new ArgumentException("Bad value to ToBoolean: " + str);
            }
        }
    }
}
