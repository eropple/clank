﻿// Copyright (c) 2009-2014 Math.NET
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

using System;
using System.Collections.Generic;
using DiceNotation.Rollers;
using MathNet.Numerics;
using MathNet.Numerics.Properties;
using MathNet.Numerics.Random;
using Newtonsoft.Json;

namespace Clank.Utils.Random
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ReloadableXorshift : RandomSource, IDieRoller
    {
        /// <summary>
        /// The default value for X1.
        /// </summary>
        const uint YSeed = 362436069;

        /// <summary>
        /// The default value for X2.
        /// </summary>
        const uint ZSeed = 77465321;

        /// <summary>
        /// The default value for the multiplier.
        /// </summary>
        const uint ASeed = 916905990;

        /// <summary>
        /// The default value for the carry over.
        /// </summary>
        const uint CSeed = 13579;

        /// <summary>
        /// The multiplier to compute a double-precision floating point number [0, 1)
        /// </summary>
        const double UlongToDoubleMultiplier = 1.0 / (uint.MaxValue + 1.0);

        /// <summary>
        /// Seed or last but three unsigned random number.
        /// </summary>
        [JsonProperty("X")]
        public ulong X { get; private set; }

        /// <summary>
        /// Last but two unsigned random number.
        /// </summary>
        [JsonProperty("Y")]
        public ulong Y { get; private set; }

        /// <summary>
        /// Last but one unsigned random number.
        /// </summary>
        [JsonProperty("Z")]
        public ulong Z { get; private set; }

        /// <summary>
        /// The value of the carry over.
        /// </summary>
        [JsonProperty("C")]
        public ulong C { get; private set; }

        /// <summary>
        /// The multiplier.
        /// </summary>
        [JsonProperty("A")]
        public ulong A { get; private set; }

        [JsonProperty("IterCount")]
        public ulong IterCount { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class using
        /// a seed based on time and unique GUIDs.
        /// </summary>
        /// <remarks>If the seed value is zero, it is set to one. Uses the
        /// value of <see cref="Control.ThreadSafeRandomNumberGenerators"/> to
        /// set whether the instance is thread safe.
        /// Uses the default values of:
        /// <list>
        /// <item>a = 916905990</item>
        /// <item>c = 13579</item>
        /// <item>X1 = 77465321</item>
        /// <item>X2 = 362436069</item>
        /// </list></remarks>
        private ReloadableXorshift()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class using
        /// a seed based on time and unique GUIDs.
        /// </summary>
        /// <param name="a">The multiply value</param>
        /// <param name="c">The initial carry value.</param>
        /// <param name="x1">The initial value if X1.</param>
        /// <param name="x2">The initial value if X2.</param>
        /// <remarks>If the seed value is zero, it is set to one. Uses the
        /// value of <see cref="Control.ThreadSafeRandomNumberGenerators"/> to
        /// set whether the instance is thread safe.
        /// Note: <paramref name="c"/> must be less than <paramref name="a"/>.
        /// </remarks>
        public ReloadableXorshift(long a, long c, long x1, long x2) : this(RandomSeed.Robust(), a, c, x1, x2)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class using
        /// a seed based on time and unique GUIDs.
        /// </summary>
        /// <param name="threadSafe">if set to <c>true</c> , the class is thread safe.</param>
        /// <remarks>
        /// Uses the default values of:
        /// <list>
        /// <item>a = 916905990</item>
        /// <item>c = 13579</item>
        /// <item>X1 = 77465321</item>
        /// <item>X2 = 362436069</item>
        /// </list></remarks>
        public ReloadableXorshift(bool threadSafe) : this(RandomSeed.Robust(), threadSafe)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class using
        /// a seed based on time and unique GUIDs.
        /// </summary>
        /// <param name="threadSafe">if set to <c>true</c> , the class is thread safe.</param>
        /// <param name="a">The multiply value</param>
        /// <param name="c">The initial carry value.</param>
        /// <param name="x1">The initial value if X1.</param>
        /// <param name="x2">The initial value if X2.</param>
        /// <remarks><paramref name="c"/> must be less than <paramref name="a"/>.</remarks>
        public ReloadableXorshift(bool threadSafe, long a, long c, long x1, long x2) : this(RandomSeed.Robust(), threadSafe, a, c, x1, x2)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        /// <remarks>If the seed value is zero, it is set to one. Uses the
        /// value of <see cref="Control.ThreadSafeRandomNumberGenerators"/> to
        /// set whether the instance is thread safe.
        /// Uses the default values of:
        /// <list>
        /// <item>a = 916905990</item>
        /// <item>c = 13579</item>
        /// <item>X1 = 77465321</item>
        /// <item>X2 = 362436069</item>
        /// </list></remarks>
        public ReloadableXorshift(int seed) : this(seed, Control.ThreadSafeRandomNumberGenerators)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        /// <remarks>If the seed value is zero, it is set to one. Uses the
        /// value of <see cref="Control.ThreadSafeRandomNumberGenerators"/> to
        /// set whether the instance is thread safe.</remarks>
        /// <param name="a">The multiply value</param>
        /// <param name="c">The initial carry value.</param>
        /// <param name="x1">The initial value if X1.</param>
        /// <param name="x2">The initial value if X2.</param>
        /// <remarks><paramref name="c"/> must be less than <paramref name="a"/>.</remarks>
        public ReloadableXorshift(int seed, long a, long c, long x1, long x2) : this(seed, Control.ThreadSafeRandomNumberGenerators, a, c, x1, x2)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        /// <param name="threadSafe">if set to <c>true</c>, the class is thread safe.</param>
        /// <remarks>
        /// Uses the default values of:
        /// <list>
        /// <item>a = 916905990</item>
        /// <item>c = 13579</item>
        /// <item>X1 = 77465321</item>
        /// <item>X2 = 362436069</item>
        /// </list></remarks>
        public ReloadableXorshift(int seed, bool threadSafe) : base(threadSafe)
        {
            if (seed == 0)
            {
                seed = 1;
            }

            X = (uint)seed;
            Y = YSeed;
            Z = ZSeed;
            C = CSeed;
            A = ASeed;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Xorshift"/> class.
        /// </summary>
        /// <param name="seed">The seed value.</param>
        /// <param name="threadSafe">if set to <c>true</c>, the class is thread safe.</param>
        /// <param name="a">The multiply value</param>
        /// <param name="c">The initial carry value.</param>
        /// <param name="x1">The initial value if X1.</param>
        /// <param name="x2">The initial value if X2.</param>
        /// <remarks><paramref name="c"/> must be less than <paramref name="a"/>.</remarks>
        public ReloadableXorshift(int seed, bool threadSafe, long a, long c, long x1, long x2) : base(threadSafe)
        {
            if (seed == 0)
            {
                seed = 1;
            }

            if (a <= c)
            {
                throw new ArgumentException(string.Format(Resources.ArgumentOutOfRangeGreater, "a", "c"), "a");
            }

            X = (uint)seed;
            Y = (ulong)x1;
            Z = (ulong)x2;
            A = (ulong)a;
            C = (ulong)c;
        }

        /// <summary>
        /// Returns a random number between 0.0 and 1.0.
        /// </summary>
        /// <returns>
        /// A double-precision floating point number greater than or equal to 0.0, and less than 1.0.
        /// </returns>
        protected override sealed double DoSample()
        {
            ++IterCount;
            var t = (A * X) + C;
            X = Y;
            Y = Z;
            C = t >> 32;
            Z = t & 0xffffffff;
            return Z * UlongToDoubleMultiplier;
        }

        /// <summary>
        /// Fills an array with random numbers greater than or equal to 0.0 and less than 1.0.
        /// </summary>
        /// <remarks>Supports being called in parallel from multiple threads.</remarks>
        [CLSCompliant(false)]
        public static void Doubles(double[] values, int seed, ulong a = ASeed, ulong c = CSeed, ulong x1 = YSeed, ulong x2 = ZSeed)
        {
            if (a <= c)
            {
                throw new ArgumentException(string.Format(Resources.ArgumentOutOfRangeGreater, "a", "c"), "a");
            }

            if (seed == 0)
            {
                seed = 1;
            }

            ulong x = (uint)seed;

            for (int i = 0; i < values.Length; i++)
            {
                var t = (a * x) + c;
                x = x1;
                x1 = x2;
                c = t >> 32;
                x2 = t & 0xffffffff;
                values[i] = x2 * UlongToDoubleMultiplier;
            }
        }

        /// <summary>
        /// Returns an array of random numbers greater than or equal to 0.0 and less than 1.0.
        /// </summary>
        /// <remarks>Supports being called in parallel from multiple threads.</remarks>
        [CLSCompliant(false)]
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public static double[] Doubles(int length, int seed, ulong a = ASeed, ulong c = CSeed, ulong x1 = YSeed, ulong x2 = ZSeed)
        {
            var data = new double[length];
            Doubles(data, seed, a, c, x1, x2);
            return data;
        }

        /// <summary>
        /// Returns an infinite sequence of random numbers greater than or equal to 0.0 and less than 1.0.
        /// </summary>
        /// <remarks>Supports being called in parallel from multiple threads, but the result must be enumerated from a single thread each.</remarks>
        [CLSCompliant(false)]
        public static IEnumerable<double> DoubleSequence(int seed, ulong a = ASeed, ulong c = CSeed, ulong x1 = YSeed, ulong x2 = ZSeed)
        {
            if (a <= c)
            {
                throw new ArgumentException(string.Format(Resources.ArgumentOutOfRangeGreater, "a", "c"), "a");
            }

            if (seed == 0)
            {
                seed = 1;
            }

            ulong x = (uint)seed;

            while (true)
            {
                var t = (a * x) + c;
                x = x1;
                x1 = x2;
                c = t >> 32;
                x2 = t & 0xffffffff;
                yield return x2 * UlongToDoubleMultiplier;
            }
        }

        public int RollDie(int sides)
        {
            return Next(1, sides + 1);
        }

        public ReloadableXorshift Clone()
        {
            return (ReloadableXorshift)MemberwiseClone();
        }
    }
}
