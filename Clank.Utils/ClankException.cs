﻿using System;

namespace Clank.Utils
{
    public class ClankException : Exception
    {
        public ClankException()
        {
        }

        public ClankException(string message)
            : base(message)
        {
        }

        public ClankException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ClankException(Exception innerException, String message, params Object[] args)
            : base(String.Format(message, args), innerException)
        { }

        public ClankException(String message, params Object[] args)
            : base(String.Format(message, args), null)
        { }
    }
}
