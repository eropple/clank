﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Clank.Pipeline
{
    public class TextureSourceContent
    {
        public readonly Texture2DContent Texture;

        public TextureSourceContent(Texture2DContent texture)
        {
            Texture = texture;
        }
    }

    [ContentImporter(".png", DisplayName = "Texture Source Importer - Clank", DefaultProcessor = "ClankTextureSourceProcessor")]
    public class ClankTextureSourceImporter : ContentImporter<String>
    {
        public override String Import(String filename, ContentImporterContext context)
        {
            return filename;
        }
    }

    [ContentProcessor(DisplayName = "Texture Source Processor - Clank")]
    public class ClankTextureSourceProcessor : ContentProcessor<String, TextureSourceContent>
    {
        public override TextureSourceContent Process(string input, ContentProcessorContext context)
        {
            var reference = new ExternalReference<TextureContent>(input);

            var data = new OpaqueDataDictionary();
            data.Add("PremultiplyAlpha", true);



            var texture = (Texture2DContent) context.BuildAndLoadAsset<TextureContent, TextureContent>(reference,
                "MGTextureProcessor", data,
                "TextureImporter");

            return new TextureSourceContent(texture);
        }
    }

    [ContentTypeWriter]
    public class TextureSourceWriter : ContentTypeWriter<TextureSourceContent>
    {
        public override string GetRuntimeReader(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
        {
            return "Clank.Graphics.Sources.TextureSourceReader, Clank";
        }

        public override string GetRuntimeType(Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform targetPlatform)
        {
            return "Clank.Graphics.Sources.TextureSource, Clank";
        }

        protected override void Write(ContentWriter output, TextureSourceContent value)
        {
            output.WriteObject((TextureContent)value.Texture);
        }
    }
}
