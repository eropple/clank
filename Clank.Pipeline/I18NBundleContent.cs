﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdCanHack.SheetParser;
using EdCanHack.SheetParser.OpenXml;
using EdCanHack.SheetParser.SpreadsheetML;
using EdCanHack.SheetParser.Transforms;
using EdCanHack.SheetParser.Transforms.Serialization;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using MoreLinq;

namespace Clank.Pipeline
{
    [SheetDeserializable(ignoreExtraFields: true, ignoreMissingFields: false)]
    public class I18NRecord : IKeyed<String>
    {
        public String Key { get; private set; }

        [SheetProperty("Value")]
        public String Value { get; private set; }
    }

    public class I18NBundleContent
    {
        public readonly List<I18NRecord> Entries;
        public I18NBundleContent(IEnumerable<I18NRecord> entries)
        {
            Entries = entries.ToList();
        }
    }

    [ContentImporter(".i18n-xml", DisplayName = "I18N Importer (Excel 2003) - Clank", DefaultProcessor = "ClankI18NProcessor")]
    public class ClankExcel2003I18NImporter : ContentImporter<INamedSheetReader>
    {
        public override INamedSheetReader Import(string filename, ContentImporterContext context)
        {
            return new SpreadsheetMLReader(filename, true);
        }
    }

    [ContentImporter(".i18n-xlsx", DisplayName = "I18N Importer (OpenXML) - Clank" ,DefaultProcessor = "ClankI18NProcessor")]
    public class ClankOpenXMLI18NImporter : ContentImporter<INamedSheetReader>
    {
        public override INamedSheetReader Import(string filename, ContentImporterContext context)
        {
            return new OpenXMLReader(filename, true);
        }
    }

    [ContentProcessor(DisplayName = "I18N Sheet Processor - Clank")]
    public class ClankI18NProcessor : ContentProcessor<INamedSheetReader, I18NBundleContent>
    {
        public override I18NBundleContent Process(INamedSheetReader input, ContentProcessorContext context)
        {
            var deserializer = new SheetDeserializer<I18NRecord>();

            var entries =
                input.SheetNames.Select(input.ReadSheet)
                    .SelectMany(sheet => deserializer.TransformRows(sheet))
                    .DistinctBy(record => record.Key);

            return new I18NBundleContent(entries);
        }
    }



    [ContentTypeWriter]
    public class I18NWriter : ContentTypeWriter<I18NBundleContent>
    {
        protected override void Write(ContentWriter output, I18NBundleContent value)
        {
            output.Write(value.Entries.Count);
            foreach (var kvp in value.Entries)
            {
                output.Write(kvp.Key);
                output.Write(kvp.Value);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Clank.Text.I18NBundleReader, Clank";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Clank.Text.I18NBundle, Clank";
        }
    }
}
