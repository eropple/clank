﻿using Microsoft.Xna.Framework.Content.Pipeline;

namespace Clank.Pipeline.Utility
{
    /// <summary>
    /// MonoGame loses its shit if there isn't a content processor available.
    /// </summary>
    /// <typeparam name="TContentType"></typeparam>
    public class PassThroughContentProcessor<TContentType> : ContentProcessor<TContentType, TContentType>
    {
        public override TContentType Process(TContentType input, ContentProcessorContext context)
        {
            return input;
        }
    }
}
