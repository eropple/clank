﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Clank.Pipeline
{
    public static class ExtensionMethods
    {
        public static void Write(this ContentWriter output, Rectangle rect)
        {
            output.Write(rect.X);
            output.Write(rect.Y);
            output.Write(rect.Width);
            output.Write(rect.Height);
        }

        public static void Write(this ContentWriter output, Dictionary<String, String> dict)
        {
            output.Write(dict.Count);
            foreach (var kvp in dict)
            {
                output.Write(kvp.Key);
                output.Write(kvp.Value);
            }
        }

        public static void WriteEnumerable<TValue>(this ContentWriter output, IEnumerable<TValue> enumerable, Action<TValue> a)
        {
            var list = enumerable.ToList();
            output.Write(list.Count);
            foreach (var item in list) a.Invoke(item);
        }
    }
}
