﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

using SDRectangle = System.Drawing.Rectangle;
using SDSize = System.Drawing.Size;
using SDPoint = System.Drawing.Point;
using SDColor = System.Drawing.Color;
using Bitmap = System.Drawing.Bitmap;
using Image = System.Drawing.Image;
using Graphics = System.Drawing.Graphics;
using Clank.Pipeline.Utility;

namespace Clank.Pipeline
{
    public class TextureAtlasInput
    {
        public readonly String Name;
        public readonly Int32 Padding;
        public readonly Dictionary<String, Bitmap> Images;

        public TextureAtlasInput(String name, Int32 padding, Dictionary<String, Bitmap> images)
        {
            Name = name;
            Padding = padding;
            Images = images;
        }
    }

    public class TextureAtlasContent
    {
        public readonly TextureSourceContent Source;
        public Dictionary<String, Rectangle> Entries;

        public TextureAtlasContent(TextureSourceContent source, Dictionary<String, Rectangle> entries)
        {
            Source = source;
            Entries = entries;
        }
    }

    [ContentImporter(".tps", DisplayName = "Atlas Importer (Texture Packer Pro) - Clank", DefaultProcessor = "ClankTextureAtlasTPPProcessor")]
    public class ClankTextureAtlasTPPImporter : ContentImporter<String>
    {
        public override String Import(string filename, ContentImporterContext context)
        {
            String tempDir = Path.Combine(context.IntermediateDirectory);
            String dataFile = Path.Combine(tempDir, Path.GetFileNameWithoutExtension(filename) + ".xml");
            String assetFile = Path.Combine(tempDir, Path.GetFileNameWithoutExtension(filename) + ".png");

            String commandLine = String.Format(
                "--format xml --texture-format png --max-width 4096 --max-height 4096 --opt RGBA8888 --disable-clean-transparency --disable-rotation " +
                "--algorithm MaxRects --maxrects-heuristics Best " +
                "--data \"{0}\" --sheet \"{1}\" \"{2}\"",
            dataFile, assetFile, filename);

            var ps = new ProcessStartInfo(ContentPaths.TexturePackerExecutable, commandLine);
            ps.UseShellExecute = false;
            var process = Process.Start(ps);
            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                throw new Exception(String.Format("Texture Packer returned error code: {0} for line: \n\n\n{1}\n\n\n", process.ExitCode, commandLine));
            }

            return dataFile;
        }
    }

    [ContentProcessor(DisplayName = "Atlas Processor (Texture Packer Pro) - Clank")]
    public class ClankTextureAtlasTPPProcessor : ContentProcessor<String, TextureAtlasContent>
    {
        public override TextureAtlasContent Process(String input, ContentProcessorContext context)
        {
            //<TextureAtlas imagePath="blerp.png" width="451" height="354">
            //    <sprite n="adam-normal.png" x="2" y="2" w="447" h="350"/>
            //</TextureAtlas>

            var entries = new Dictionary<String, Rectangle>();

            XDocument xml = XDocument.Load(input);
            foreach (XElement sprite in xml.Root.Elements("sprite"))
            {
                String name = sprite.Attribute("n").Value;
                Rectangle rect = new Rectangle(Int32.Parse(sprite.Attribute("x").Value), Int32.Parse(sprite.Attribute("y").Value),
                                               Int32.Parse(sprite.Attribute("w").Value), Int32.Parse(sprite.Attribute("h").Value));

                entries.Add(name, rect);
            }

            var ext = new ExternalReference<String>(Path.Combine(context.IntermediateDirectory, xml.Root.Attribute("imagePath").Value));
            TextureSourceContent source = context.BuildAndLoadAsset<String, TextureSourceContent>(ext, "ClankTextureSourceProcessor", null, "ClankTextureSourceImporter");

            return new TextureAtlasContent(source, entries);
        }
    }

    [ContentImporter(".atlas", DisplayName = "Atlas Importer (MaxRects) - Clank", DefaultProcessor = "ClankTextureAtlasBitmapDictionaryProcessor")]
    public class ClankTextureAtlasMRImporter : ContentImporter<TextureAtlasInput>
    {
        public override TextureAtlasInput Import(string filename, ContentImporterContext context)
        {
            var baseDir = Path.GetDirectoryName(filename);

            // remove blanks and comments
            var lines = File.ReadAllLines(filename).Select(line => line.Trim()).Where(line => line.Length > 0 && line[0] != '#').ToList();

            var optionLines = lines.TakeWhile(line => line[0] == '@').Select(line => line.ToLowerInvariant()).ToList();
            var recursive = optionLines.Contains("@recursive");

            var globLines = lines.SkipWhile(line => line[0] == '@').Select(line => line.Replace('\\', '/')).ToList();

            var imageFilenames = globLines.SelectMany(glob => Directory.GetFiles(baseDir, glob, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)).Distinct().ToList();

            var dict = new Dictionary<String, Bitmap>(imageFilenames.Count);
            foreach (var imageFilename in imageFilenames)
            {
                try
                {
                    var img = Image.FromFile(imageFilename) as Bitmap;
                    if (img == null) throw new FormatException("Image is not a bitmap!");

                    var key = imageFilename.Replace(baseDir, "");
                    if (key[0] == '/' || key[0] == '\\') key = key.Substring(1);

                    dict[key.Replace('\\', '/')] = img;
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed on image '" + imageFilename + "'.", ex);
                }
            }

            // TODO: make padding configurable
            return new TextureAtlasInput(Path.GetFileNameWithoutExtension(filename), 1, dict);
        }
    }

    [ContentProcessor(DisplayName = "Atlas Processor (MaxRects) - Clank")]
    public class ClankTextureAtlasBitmapDictionaryProcessor : ContentProcessor<TextureAtlasInput, TextureAtlasContent>
    {
        public override TextureAtlasContent Process(TextureAtlasInput input, ContentProcessorContext context)
        {
            var packed = GetPlacedPaddedRectangles(input.Images, 8, 8, input.Padding);

            var bitmap = new Bitmap(packed.Item1.Width, packed.Item1.Height);
            var graphics = Graphics.FromImage(bitmap);

            graphics.FillRectangle(new System.Drawing.SolidBrush(SDColor.Transparent), 0, 0, packed.Item1.Width, packed.Item1.Height);

            var entries = new Dictionary<String, Rectangle>(input.Images.Count);
            foreach (var image in input.Images)
            {
                var packedRect = packed.Item2[image.Key];
                var drawPoint = new SDPoint(packedRect.X + input.Padding, packedRect.Y + input.Padding);
                var realRect = new Rectangle(drawPoint.X, drawPoint.Y, image.Value.Width, image.Value.Height);

                graphics.DrawImageUnscaled(image.Value, drawPoint);
                entries[image.Key] = realRect;
            }
            graphics.Flush(System.Drawing.Drawing2D.FlushIntention.Sync);

            var tempFileName = Path.Combine(context.IntermediateDirectory, input.Name + ".png");
            bitmap.Save(tempFileName);

            var ext = new ExternalReference<String>(tempFileName);
            TextureSourceContent source = context.BuildAndLoadAsset<String, TextureSourceContent>(ext, "ClankTextureSourceProcessor", null, "ClankTextureSourceImporter");
            return new TextureAtlasContent(source, entries);
        }

        private Tuple<SDSize, Dictionary<String, SDRectangle>> GetPlacedPaddedRectangles(Dictionary<String, Bitmap> bitmaps, Int32 width, Int32 height, Int32 padding, Boolean bounce = true)
        {
            if (width > 8192 || height > 8192) throw new Exception("Tried to build an atlas with a dimension > 8192.");

            var bin = new BinPacker(width, height);
            var bitmapRects = bitmaps.Select(kvp => Tuple.Create(kvp.Key, new SDRectangle(0, 0, kvp.Value.Width + (padding * 2), kvp.Value.Height + (padding * 2))))
                //                .OrderBy(o => o.Item2.Width) // TODO: experiment for best order
                //                .ThenBy(o => o.Item2.Height)
                .ToList();

            var dict = new Dictionary<String, SDRectangle>(bitmapRects.Count);
            foreach (var bitmapRect in bitmapRects)
            {
                var binnedRect = bin.Insert(bitmapRect.Item2, BinPacker.FreeRectChoiceHeuristic.RectBestShortSideFit);
                
                if (binnedRect.Width == 0 || binnedRect.Height == 0)
                {
                    return GetPlacedPaddedRectangles(bitmaps, width * (bounce ? 2 : 1), height * (bounce ? 1 : 2), padding, !bounce);
                }

                dict[bitmapRect.Item1] = binnedRect;
            }
            return Tuple.Create(new SDSize(width, height), dict);
        }
    }


    [ContentTypeWriter]
    public class TextureAtlasWriter : ContentTypeWriter<TextureAtlasContent>
    {
        protected override void Write(ContentWriter output, TextureAtlasContent value)
        {
            output.WriteObject(value.Source);
            output.Write(value.Entries.Count);
            foreach (String key in value.Entries.Keys)
            {
                output.Write(key);
                output.Write(value.Entries[key]);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Clank.Graphics.TextureAtlasReader, Clank";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "Clank.Graphics.TextureAtlas, Clank";
        }
    }
}
