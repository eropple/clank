﻿using Exor.Compiler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Versioner;

namespace Clank.Extensibility
{
    [JsonObject(MemberSerialization.OptIn)]
#if CAP_DYNAMIC_COMPILER
    public class ModMetadata : ICodeSource
#else
    public class ModMetadata : IVersioned, IDepending
#endif
    {
        [JsonProperty("UniqueName")]
        public String UniqueName { get; private set; }

        [JsonProperty("Version")]
        [JsonConverter(typeof(VersionStringConverter))]
        public Versioner.Version Version { get; private set; }

        [JsonProperty("Dependencies")]
        public IEnumerable<Versioner.Dependency> Dependencies { get; private set; }

        public String Location { get; internal set; }

        public override string ToString()
        {
            return String.Format("{0} v{1}", UniqueName, Version);
        }

#if CAP_DYNAMIC_COMPILER
        [JsonProperty("AdditionalAssemblies")]
        public IEnumerable<string> AdditionalAssembliesRaw { get; private set; }

        public IEnumerable<String> AdditionalAssemblies => 
            AdditionalAssembliesRaw.Select(a => Path.Combine(
                Location,
                a
            ));

        public IEnumerable<String> CodeFiles => 
            Directory.GetFiles(Path.Combine(Location, "Scripts"), "*.cs", SearchOption.AllDirectories);
#endif
    }
}
