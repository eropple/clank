﻿using Clank.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using Clank.Utils;
using Common.Logging;
using Exor.Compiler;
using Exor.Core;
using MoreLinq;

namespace Clank.Extensibility
{
    public class ModManager
    {
        private readonly ILog _logger;
        public IReadOnlyList<ModMetadata> SelectedMods { get; private set; }
        public IReadOnlyDictionary<String, ModMetadata> AllMods { get; private set; }

        private ExtensionLoader _extensions;
        public ExtensionLoader Extensions => _extensions = _extensions ?? BuildExtensionLoader();

        private readonly ClankConfiguration _config;
        private readonly IReadOnlyList<ExtensionTypeRecord> _extensionTypeRecords;

        internal ModManager(GameBase game, IReadOnlyList<ExtensionTypeRecord> extensionTypeRecords)
        {
            _config = game.ClankConfiguration;
            _logger = game.Logging.GetLogger<ModManager>();
            _extensionTypeRecords = extensionTypeRecords;
            AllMods = GetAllMods(_config);

            var missingMods = _config.Mods.SelectedMods.Where(a => !AllMods.ContainsKey(a)).ToList();
            if (missingMods.Count > 0)
            {
                throw new ClankException("Required mods missing: " + String.Join(", ", missingMods));
            }
            SelectedMods = _config.Mods.SelectedMods.Select(a => AllMods[a]).ToList();
        }

        public void LoadExtensions()
        {
            if (Extensions != null) return;

            _extensions = BuildExtensionLoader();
        }

        [SuppressMessage("ReSharper", "AccessToForEachVariableInClosure")]
        private ExtensionLoader BuildExtensionLoader()
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            var universalAssemblies = _config.Mods.UniversalAssemblies.Concat(Assembly.GetExecutingAssembly().Location);
            if (entryAssembly != null) universalAssemblies = universalAssemblies.Concat(entryAssembly.Location).Concat("MonoGame.Framework.dll");

            var compiler = new CSharpCompiler<ModMetadata>(new CSharpCompilerOptions(forceRecompile: true,
                universalAssemblies: universalAssemblies,
                cachePath: Paths.ModCacheRoot));

            try
            {
                List<ExorCompilerResults<ModMetadata>> results;
                if (!compiler.TryCompileSources(SelectedMods, out results))
                {
                    foreach (var result in results.Where(r => r.Failed))
                    {
                        _logger.FatalFormat("Compilation of '0' FAILED:", result.Source);
                        foreach (var error in result.Errors)
                        {
                            _logger.InfoFormat("  in file {0}:", error.FileName);

                            var msg = String.Format("    ({0}, {1}): {2}", error.Line, error.Column, error.ErrorText);

                            if (error.IsWarning)
                                _logger.Warn(msg);
                            else
                                _logger.Error(msg);
                        }
                    }
#if DEBUG
                    Debugger.Break();
#endif
                }

                var resultsMap = results.ToDictionary(r => r.Source.UniqueName);
                var loadOrder = SelectedMods
                    .Select(m => resultsMap[m.UniqueName].Assembly)
                    .Concat(_config.Mods.AppendedExtensionAssemblies.Select(Assembly.Load));



                if (entryAssembly != null) loadOrder = loadOrder.Concat(entryAssembly);

                return new ExtensionLoader(_logger, loadOrder, _extensionTypeRecords);
            }
            catch (Exception ex)
            {
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        private static Dictionary<String, ModMetadata> GetAllMods(ClankConfiguration config)
        {
            var retval = new Dictionary<String, ModMetadata>();

            var serializer = new JsonSerializer();

            foreach (var contentDirectory in config.Mods.ContentDirectories)
            {
                foreach (var modDirectory in Directory.GetDirectories(contentDirectory))
                {
                    var infoFile = Path.Combine(modDirectory, "info.json");
                    if (!File.Exists(infoFile)) continue;

                    var metadata = serializer.Deserialize<ModMetadata>(new JsonTextReader(File.OpenText(infoFile)));
                    if (retval.ContainsKey(metadata.UniqueName)) continue;

                    metadata.Location = modDirectory;
                    retval[metadata.UniqueName] = metadata;
                }
            }

            return retval;
        }
    }
}
