﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Utils;

namespace Clank.Extensibility
{
    public class ModException : ClankException
    {
        public ModException()
        {
        }

        public ModException(string message) : base(message)
        {
        }

        public ModException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ModException(Exception innerException, string message, params object[] args) : base(innerException, message, args)
        {
        }

        public ModException(string message, params object[] args) : base(message, args)
        {
        }
    }
}
