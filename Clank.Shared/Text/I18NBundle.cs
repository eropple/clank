﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Clank.Content;
using Common.Logging;
using Microsoft.Xna.Framework.Content;

namespace Clank.Text
{
    public class I18NBundle
    {
        public readonly IReadOnlyList<Tuple<String, String>> Entries;

        public I18NBundle(IEnumerable<Tuple<String, String>> entries)
        {
            Entries = entries.ToList();
        }
    }

    public class I18NBundleReader : BaseContentTypeReader<I18NBundle>
    {
        protected override I18NBundle DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance)
        {
            Int32 count = input.ReadInt32();
            List<Tuple<String, String>> entries = new List<Tuple<string, string>>(count);

            for (Int32 i = 0; i < count; ++i)
            {
                entries.Add(Tuple.Create(input.ReadString(), input.ReadString()));
            }

            return new I18NBundle(entries);
        }
    }
}