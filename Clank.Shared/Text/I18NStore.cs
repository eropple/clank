﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Clank.Content;
using Clank.Utility;
using Common.Logging;

namespace Clank.Text
{
    public class I18NStore
    {
        private static readonly ILog Logger = LogManager.GetLogger<I18NStore>();

        public I18NStore Parent { get; }
        public String Language { get; }

        private readonly Dictionary<String, String> _translations;

        public I18NStore(ClankContentManager content, String language, String bundlePath)
            : this(content, language, bundlePath.Yield())
        { }
        public I18NStore(I18NStore parent, ClankContentManager content, String bundlePath)
            : this(parent, content, bundlePath.Yield()) { }

        public I18NStore(ClankContentManager content, String language, IEnumerable<String> bundlePaths)
            : this(null, language, bundlePaths.SelectMany(p => content.LoadAll<I18NBundle>(String.Format("I18N/{0}/{1}", language, p))))
        { }

        // TODO: this may create a list that's not right (all of one bundle, then all of another, regardless of priority order). investigate later.
        public I18NStore(I18NStore parent, ClankContentManager content, IEnumerable<String> bundlePaths)
            : this(parent, parent.Language, 
                   bundlePaths.SelectMany(p => content.LoadAll<I18NBundle>(String.Format("I18N/{0}/{1}", parent.Language, p)))) { }

        public I18NStore(I18NStore parent, String language, IEnumerable<I18NBundle> bundles)
        {
            Parent = parent;
            Language = language;

            var bundleList = bundles.ToList();

            _translations = new Dictionary<String, String>(bundleList.Sum(b => b.Entries.Count));
            foreach (var bundle in bundleList)
            {
                foreach (var entry in bundle.Entries)
                {
                    if (!_translations.ContainsKey(entry.Item1)) _translations.Add(entry.Item1, entry.Item2);
                }
            }
        }


        public String Translate(String key, params Object[] args)
        {
            String value;
            return TryTranslate(key, out value, args) ? value : "X: " + key;
        }

        public Boolean TryTranslate(String key, out String value, params Object[] args)
        {
            value = String.Empty;
            if (key.Length == 0)
            {
                return true;
            }

            if (_translations.TryGetValue(key, out value))
            {
                value = String.Format(value, args);
                return true;
            }

            if (Parent != null) return Parent.TryTranslate(key, out value, args);
            return false;
        }
    }
}
