﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank
{
    public interface IExtendedDisposable : IDisposable
    {
        Boolean IsDisposed { get; }
    }
}
