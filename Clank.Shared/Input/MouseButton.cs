﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.Input
{
    public enum MouseButton
    {
        Left,
        Middle,
        Right,
        XButton1,
        XButton2
    }
}
