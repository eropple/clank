﻿using Clank.Utils;
using Common.Logging.Configuration;
using Clank.Utility;
using System.IO;
using Common.Logging.Simple;
using Exor.Core;
using Clank.Text;
using Clank.Config;
using Clank.Content;
using Clank.ControlFlow;
using Clank.Extensibility;
using Clank.Input;
using Common.Logging;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
#if WINDOWS && !TEST
using System.Windows.Forms;
#endif

namespace Clank
{
    public abstract class GameBase : Microsoft.Xna.Framework.Game
    {
        public readonly GraphicsDeviceManager GraphicsDeviceManager;
        public StateManager StateManager { get; private set; }
        public InputManager InputManager { get; private set; }
        public ModManager ModManager { get; private set; }
        public ClankConfiguration ClankConfiguration { get; private set; }
        public ClankContentManager GlobalContentManager { get; private set; }
        public ILogManager Logging { get; private set; }
        private readonly ILog _logger;

        public I18NStore GlobalI18NStore { get; private set; }

        protected GameBase(String windowTitle)
        {
            Logging = new LogManager();
            LoadLogger();
            _logger = Logging.GetLogger<GameBase>();

            ClankConfiguration = ClankConfiguration.GetConfiguration();

            GraphicsDeviceManager = new GraphicsDeviceManager(this);

            GraphicsDeviceManager.SupportedOrientations = DisplayOrientation.LandscapeLeft;
            GraphicsDeviceManager.PreferredBackBufferWidth = ClankConfiguration.Graphics.WindowWidth;
            GraphicsDeviceManager.PreferredBackBufferHeight = ClankConfiguration.Graphics.WindowHeight;
            GraphicsDeviceManager.IsFullScreen = ClankConfiguration.Graphics.IsFullscreen;

            GraphicsDeviceManager.SynchronizeWithVerticalRetrace = true;
            GraphicsDeviceManager.PreferMultiSampling = false;

            Window.Title = windowTitle;

#if DESKTOP
#if WINDOWS && !TEST
            var form = Control.FromHandle(Window.Handle) as System.Windows.Forms.Form;
            if (form == null) throw new ClankException("No form found for window handle.");
            var screen = Screen.FromControl(form);
            form.Location = new System.Drawing.Point(
                screen.Bounds.Location.X + 20,
                screen.Bounds.Location.Y + 20
            );
            form.FormClosing += (sender, e) =>
            {
                e.Cancel = !StateManager.TopState.DoClose();
            };
            form.Show();
            form.Focus();
            form.Activate();
#else
#error must implement.
#endif
#endif

            IsMouseVisible = true;
        }

        protected abstract State FirstState { get; }
        protected abstract IReadOnlyList<ExtensionTypeRecord> ExtensionTypeRecords { get; } 

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected sealed override void LoadContent()
        {
            ModManager = new ModManager(this, ExtensionTypeRecords);
            GlobalContentManager = new ClankContentManager(Services, this);

            GlobalI18NStore = new I18NStore(GlobalContentManager, ClankConfiguration.Language, "general");

            DoLoadContent();

            InputManager = new InputManager(this);
            StateManager = new StateManager(this);
            StateManager.Push(FirstState);
        }

        protected abstract void DoLoadContent();

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _logger.Trace(m => m("Update starts: gameTime = {0} ms", gameTime.ElapsedGameTime.TotalMilliseconds));

#if DEBUG
            if (_logger.IsTraceEnabled)
            {
                DateTime start = DateTime.UtcNow;

                base.Update(gameTime);
                InputManager.Update();
                StateManager.Update(gameTime);

                DateTime end = DateTime.UtcNow;
                _logger.Trace(m => m("Update ends: duration = {0} ms", (end - start).TotalMilliseconds));
            }
            else
            {
#endif // DEBUG
                base.Update(gameTime);
                InputManager.Update();
                StateManager.Update(gameTime);
#if DEBUG
            }
#endif // DEBUG
        }
        /// <summary>
        /// Draws the game from background to foreground.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            _logger.Trace(m => m("Draw starts: gameTime = {0} ms", gameTime.ElapsedGameTime.TotalMilliseconds));

#if DEBUG
            if (_logger.IsTraceEnabled)
            {
                DateTime start = DateTime.UtcNow;

                base.Draw(gameTime);
                StateManager.Draw(gameTime);

                DateTime end = DateTime.UtcNow;
                _logger.Trace(m => m("Draw ends: duration = {0} ms", (end - start).TotalMilliseconds));
            }
            else
            {
#endif // DEBUG
                base.Draw(gameTime);
                StateManager.Draw(gameTime);
#if DEBUG
            }
#endif // DEBUG
        }

        private void LoadLogger()
        {
#if DESKTOP
            var userConfig = Path.Combine(Paths.UserRoot, String.Format("NLog.{0}.config", StringUtils.PlatformString));
            var baseConfig = Path.Combine(Paths.ResourceRoot, String.Format("NLog.{0}.config", StringUtils.PlatformString));

            var nvc = new NameValueCollection();
            nvc.Add("configType", "FILE");
            nvc.Add("configFile", File.Exists(userConfig) ? userConfig : baseConfig);
            LogManager.Adapter = new Common.Logging.NLog.NLogLoggerFactoryAdapter(nvc);
#else
#error must implement.
#endif
        }
    }
}
