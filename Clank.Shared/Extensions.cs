﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Clank.Utility;

namespace Clank
{
    public static class Extensions
    {
        public static Vector3 ToVector3(this Vector2 v)
        {
            return new Vector3(v.X, v.Y, 0);
        }

        public static Point ToPoint(this Vector2 v)
        {
            return new Point((Int32)v.X, (Int32)v.Y);
        }

        public static TValue GetOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key)
        {
            TValue v;
            return dict.TryGetValue(key, out v) ? v : default(TValue);
        }

        public static Color Lighten(this Color c, float amount = 0.2f)
        {
            // TODO: investigate HSL lightening
            var scale = 1.0f + MathHelper.Clamp(amount, 0, 1);
            var r = MathHelper.Clamp((Int32)Math.Round(c.R * scale), 0, Byte.MaxValue);
            var g = MathHelper.Clamp((Int32)Math.Round(c.G * scale), 0, Byte.MaxValue);
            var b = MathHelper.Clamp((Int32)Math.Round(c.B * scale), 0, Byte.MaxValue);
            return Color.FromNonPremultiplied(r, g, b, c.A);
        }
        public static Color Darken(this Color c, float amount = 0.2f)
        {
            // TODO: investigate HSL lightening
            var scale = 1.0f - MathHelper.Clamp(amount, 0, 1);
            var r = MathHelper.Clamp((Int32)Math.Round(c.R * scale), 0, Byte.MaxValue);
            var g = MathHelper.Clamp((Int32)Math.Round(c.G * scale), 0, Byte.MaxValue);
            var b = MathHelper.Clamp((Int32)Math.Round(c.B * scale), 0, Byte.MaxValue);
            return Color.FromNonPremultiplied(r, g, b, c.A);
        }

        public static Vector2 ToVector2(this Point p) { return new Vector2(p.X, p.Y); }
    }
}
