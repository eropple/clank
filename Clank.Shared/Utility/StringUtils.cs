﻿using System;

namespace Clank.Utility
{
    public static class StringUtils
    {
        public static String toString<T>(T obj)
            where T : class
        {
            return (obj != null) ? obj.ToString() : "null";
        }

#if WINDOWS
        public static String PlatformString => "Windows";
#else
#error must implement.
#endif
    }
}

