﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Clank.Utility
{
    public static class AppInfo
    {
        public static readonly String Version;
        public static readonly String Copyright;

        static AppInfo()
        {
#if DESKTOP
            Version = 
                Assembly.GetEntryAssembly()
                    ?.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion ??
                Assembly.GetEntryAssembly()
                    ?.GetCustomAttribute<AssemblyVersionAttribute>()?.Version ?? "Failed to locate version.";

            Copyright =
                Assembly.GetEntryAssembly()
                    ?.GetCustomAttribute<AssemblyCopyrightAttribute>()?.Copyright ?? "Failed to locate copyright.";
#else
#error must implement
#endif
        }
    }
}
