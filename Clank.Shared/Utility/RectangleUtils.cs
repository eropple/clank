﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Clank.Utility
{
    public static class RectangleUtils
    {
        public static Rectangle Translate(this Rectangle r, Vector2 v)
        {
            return new Rectangle((int)(r.X + v.X), (int)(r.Y + v.Y), r.Width, r.Height);
        }

        public static Vector2 CalculateBoundingTranslation(this Rectangle bounder, Rectangle bounded)
        {
            if (bounder.Contains(bounded)) return Vector2.Zero;

            var delta = Vector2.Zero;

            if (bounded.Top < bounder.Top)
            {
                delta.Y = bounder.Y - bounded.Y;
            }
            else if (bounded.Bottom > bounder.Bottom)
            {
                delta.Y = bounder.Bottom - bounded.Bottom;
            }

            if (bounded.Left < bounder.Left)
            {
                delta.X = bounder.X - bounded.X;
            }
            else if (bounded.Right > bounder.Right)
            {
                delta.X = bounder.Right - bounded.Right;
            }

            return delta;
        }
    }
}
