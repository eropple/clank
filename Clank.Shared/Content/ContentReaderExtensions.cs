﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Clank.Content
{
    public static class ContentReaderExtensions
    {
        public static Rectangle ReadRectangle(this ContentReader content)
        {
            return new Rectangle(content.ReadInt32(), content.ReadInt32(), content.ReadInt32(), content.ReadInt32());
        }

        public static Dictionary<String, String> ReadStringStringDictionary(this ContentReader content)
        {
            var count = content.ReadInt32();
            var dict = new Dictionary<String, String>(count);
            for (var i = 0; i < count; ++i) dict.Add(content.ReadString(), content.ReadString());
            return dict;
        }

        public static List<String> ReadStringList(this ContentReader content)
        {
            var count = content.ReadInt32();
            var list = new List<String>(count);
            for (var i = 0; i < count; ++i) list.Add(content.ReadString());
            return list;
        }

        public static List<TValue> ReadEnumerable<TValue>(this ContentReader content, Func<TValue> func)
        {
            var count = content.ReadInt32();
            var list = new List<TValue>(count);
            for (var i = 0; i < count; ++i)
            {
                list.Add(func.Invoke());
            }

            return list;
        } 
    }
}
