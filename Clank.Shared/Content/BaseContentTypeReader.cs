﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Logging;
using Microsoft.Xna.Framework.Content;

namespace Clank.Content
{
    public abstract class BaseContentTypeReader<TContentType> : ContentTypeReader<TContentType>
    {
        protected ILog Logger { get; private set; }

        public sealed override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public sealed override int GetHashCode()
        {
            return base.GetHashCode();
        }

        protected sealed override void Initialize(ContentTypeReaderManager manager)
        {
            base.Initialize(manager);
        }

        public sealed override string ToString()
        {
            return base.ToString();
        }

        protected override object Read(ContentReader input, object existingInstance)
        {
            var cm = input.ContentManager as ClankContentManager ??
                     (input.ContentManager as ClankChildContentManager)?.ParentContentManager;
            Logger = Logger ?? cm.Logging.GetLogger(GetType());
            return DoRead(input, cm, existingInstance);
        }

        protected sealed override TContentType Read(ContentReader input, TContentType existingInstance)
        {
            return (TContentType)Read(input, (object)existingInstance);
        }

        protected abstract TContentType DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance);
    }
}
