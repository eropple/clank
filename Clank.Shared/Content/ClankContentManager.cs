﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Common.Logging;
using System.Collections.Generic;
using Clank.Extensibility;

namespace Clank.Content
{
    public class ClankContentManager : ContentManager
    {
        public readonly Int32 ID;
        private readonly ClankContentManager _parent;

        private readonly Dictionary<Type, Dictionary<String, Object>> _contentDict;

        private readonly ClankChildContentManager[] _internalContentManagers;

        public readonly GameBase Game;
        public readonly ModManager ModManager;

        private readonly ILog _logger;
        public ILogManager Logging { get; private set; }

        public ClankContentManager(IServiceProvider serviceProvider, GameBase game)
            : base(serviceProvider, Paths.AssetRoot /* unused */)
        {
            ID = GetIDAndIncrement();
            Game = game;
            Logging = Game.Logging;
            _logger = Logging.GetLogger<ClankContentManager>();

            _parent = null;
            ModManager = Game.ModManager;


            _contentDict = new Dictionary<Type, Dictionary<String, Object>>();
            var mods = ModManager.SelectedMods;
            var modNames = mods.Select(m => m.UniqueName);
            _internalContentManagers =
               modNames.Select(m => new ClankChildContentManager(this, m)).ToArray();

            _logger.Debug(m => m("Created new ClankContentManager, ID #{0}.", ID));
        }

        public ClankContentManager(ClankContentManager parent)
            : this(parent.ServiceProvider, parent.Game)
        {
            _parent = parent;
            _logger.Debug(m => m("New TSContentManager with ID #{0} is a child of ContentManager ID #{1}", ID, _parent.ID));
        }

        public override TAssetType Load<TAssetType>(string assetName)
        {
            TAssetType asset;
            if (TryGetLoadedAsset(assetName, out asset, true))
                return asset;

#if DESKTOP
            Boolean loaded = false;
            for (Int32 i = 0; i < _internalContentManagers.Length; ++i)
            {
                if (_internalContentManagers[i].Exists(assetName))
                {
                    // IMPORTANT AS HECK:
                    // If this gives you a NullReferenceException, it is probably because you have the
                    // wrong types described in your ContentTypeWriter's GetRuntimeType() and GetRuntimeReader()
                    // methods.
                    asset = _internalContentManagers[i].Load<TAssetType>(assetName);
                    _logger.Debug(m => m("Loaded '{0}' from mod '{1}'.", assetName, ModManager.SelectedMods[i].UniqueName));
                    loaded = true;
                    break;
                }
            }
            if (!loaded) throw new ClankContentException("Could not load '{0}' from any child content manager.", assetName);
#else
#error TODO: implement asset loading
#endif

            Dictionary<String, Object> internalDict;
            if (!_contentDict.TryGetValue(typeof(TAssetType), out internalDict))
            {
                internalDict = new Dictionary<string, object>();
                _contentDict.Add(typeof(TAssetType), internalDict);
            }
            internalDict.Add(assetName, asset);

            return asset;
        }

        public virtual List<TAssetType> LoadAll<TAssetType>(String assetName)
        {
            return
                _internalContentManagers.Where(c => c.Exists(assetName))
                    .Select(c => c.Load<TAssetType>(assetName))
                    .ToList();
        }

        public Boolean Exists<TAssetType>(String assetName)
        {
            TAssetType asset;
            if (TryGetLoadedAsset(assetName, out asset, true)) return true;

            return _internalContentManagers.Any(c => c.Exists(assetName));
        }

        public Boolean TryGetLoadedAsset<TAssetType>(String key, out TAssetType retval, Boolean recursive = false)
        {
            Dictionary<String, Object> internalDict;

            Object asset;
            if (_contentDict.TryGetValue(typeof(TAssetType), out internalDict))
            {
                if (internalDict.TryGetValue(key, out asset))
                {
                    if (asset is TAssetType)
                    {
                        retval = (TAssetType)asset;
                        return true;
                    }

                    retval = default(TAssetType);
                    return false;
                }
            }
            else if (recursive && _parent != null)
            {
                TAssetType a2;
                if (_parent.TryGetLoadedAsset(key, out a2, true))
                {
                    retval = a2;
                    return true;
                }
                retval = default(TAssetType);
                return false;
            }

            retval = default(TAssetType);
            return false;
        }



        private static readonly Object _managerIdLock = new Object();
        private static Int32 _managerId = 0;
        public static ClankContentManager Global;

        private static Int32 GetIDAndIncrement()
        {
            lock (_managerIdLock)
            {
                return _managerId++;
            }
        }
    }
}

