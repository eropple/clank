﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Utils;
using Microsoft.Xna.Framework.Content;

namespace Clank.Content
{
    public class ClankContentException : ClankException
    {
        public ClankContentException()
        {
        }

        public ClankContentException(string message) : base(message)
        {
        }

        public ClankContentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ClankContentException(Exception innerException, string message, params object[] args) : base(innerException, message, args)
        {
        }

        public ClankContentException(string message, params object[] args) : base(message, args)
        {
        }
    }
}
