﻿using Microsoft.Xna.Framework.Content;
using System;
using System.IO;

namespace Clank.Content
{
    public class ClankChildContentManager : ContentManager
    {
        public readonly ClankContentManager ParentContentManager;

        public ClankChildContentManager(ClankContentManager parentContentManager, String modName)
            : base(parentContentManager.ServiceProvider, Path.Combine(parentContentManager.RootDirectory, modName))
        {
            ParentContentManager = parentContentManager;
        }

#if DESKTOP
        public Boolean Exists(String assetName)
        {
            return File.Exists(Path.Combine(RootDirectory, assetName + ".xnb"));
        }

        protected override Stream OpenStream(string assetName)
        {
            try
            {
                var xnbPath = Path.Combine(this.RootDirectory, assetName) + ".xnb";
                if (!File.Exists(xnbPath)) throw new FileNotFoundException("Content not found.", xnbPath);
                return File.OpenRead(xnbPath);
            }
            catch (FileNotFoundException ex)
            {
                throw new ContentLoadException("The content file was not found.", (Exception) ex);
            }
            catch (Exception ex)
            {
                throw new ContentLoadException("Opening stream error.", ex);
            }
        }
#else
#error Implement asset location.
#endif
    }
}