﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.Content
{
    public interface ILoadRecordProvider
    {
        IEnumerable<LoadRecord> ContentRecords { get; }
    }
}
