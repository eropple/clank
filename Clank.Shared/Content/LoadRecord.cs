﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Clank.Utility;
using NLog;

namespace Clank.Content
{
    public class LoadRecord<TContentType> : LoadRecord
    {
        public LoadRecord(String assetName, Boolean loadAll = false)
        {
            AssetName = assetName;
            LoadAll = loadAll;
        }

        public override String AssetName { get; }

        public override Boolean LoadAll { get; }

        public override Type Type => typeof(TContentType);
    }

    public abstract class LoadRecord
    {
        public abstract Type Type { get; }
        public abstract String AssetName { get; }

        public abstract Boolean LoadAll { get; }

        public void PreloadFromRecord(ClankContentManager content)
        {
            var genericMethod = (LoadAll ? _loadAllMethod : _loadMethod).MakeGenericMethod(new[] { Type });

            // This is a little wacky, but makes this code easier to deal with, as we have to check for
            // ILoadRecordProviders and recursively load into them.
            var o = genericMethod.Invoke(content, new Object[] {AssetName});
            var enumerable =  o as IEnumerable ?? o.Yield();

            foreach (var dep in enumerable.OfType<ILoadRecordProvider>().SelectMany(d => d.ContentRecords))
            {
                dep.PreloadFromRecord(content);
            }
        }

        private static readonly MethodInfo _loadMethod;
        private static readonly MethodInfo _loadAllMethod;

        static LoadRecord()
        {
            _loadMethod = typeof(ClankContentManager).GetTypeInfo().GetMethod("Load", new[] { typeof(String) });
            _loadAllMethod = typeof(ClankContentManager).GetTypeInfo().GetMethod("LoadAll", new[] { typeof(String) });
        }
    }
}
