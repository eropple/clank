﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Clank
{
#if DESKTOP
    public static class Paths
    {
        public static readonly String ExecutableRoot;
        public static readonly String ResourceRoot;
        public static readonly String AssetRoot;

        public static readonly String UserRoot;
        public static readonly String SavesRoot;

        public static readonly String UserLocalRoot;
        public static readonly String ModCacheRoot;
        public static readonly String ModsRoot;
        public static readonly String LogsRoot;

        public static readonly String BaseConfigPath;
        public static readonly String UserConfigPath;

        private static readonly Dictionary<String, String> TokenDictionary;
        public static String SubstituteTokens(String path)
        {
            return TokenDictionary.Aggregate(path, (current, kvp) => current.Replace(kvp.Key, kvp.Value));
        }

        static Paths()
        {
#if WINDOWS
            ExecutableRoot = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            ResourceRoot = ExecutableRoot;

            var cAttr = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyCompanyAttribute>();
            var pAttr = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyProductAttribute>();
            if (cAttr == null || String.IsNullOrWhiteSpace(cAttr.Company) ||
                pAttr == null || String.IsNullOrWhiteSpace(pAttr.Product))
            {
                throw new Exception("Entry assembly must have content in its Company and Product attributes.");
            }

            UserRoot = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                cAttr.Company,
                pAttr.Product);
            UserLocalRoot = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                cAttr.Company,
                pAttr.Product);
#else
#error must implement
#endif
            AssetRoot = Path.Combine(ResourceRoot, "Assets");
            Directory.CreateDirectory(AssetRoot);
            SavesRoot = Path.Combine(UserRoot, "Saves");
            Directory.CreateDirectory(SavesRoot);

            ModCacheRoot = Path.Combine(UserLocalRoot, "ModCache");
            ModsRoot = Path.Combine(UserLocalRoot, "Mods");
            Directory.CreateDirectory(ModsRoot);
            LogsRoot = Path.Combine(UserLocalRoot, "Logs");
            Directory.CreateDirectory(LogsRoot);

            BaseConfigPath = Path.Combine(ResourceRoot, "config.json");
            UserConfigPath = Path.Combine(UserRoot, "config.json");

            TokenDictionary = new Dictionary<string, string>
            {
                { "${EXECUTABLE_ROOT}", ExecutableRoot },
                { "${RESOURCE_ROOT}", ResourceRoot },
                { "${ASSET_ROOT}", AssetRoot },
                { "${SAVES_ROOT}", SavesRoot },
                { "${MODS_ROOT}",  ModsRoot }
            };
        }
    }
#endif
}
