﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Clank.Primitives
{
    [JsonObject(MemberSerialization.OptIn)]
    public struct Position
    {
        [JsonProperty]
        public Int32 X { get; private set; }
        [JsonProperty]
        public Int32 Y { get; private set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static implicit operator Point(Position p)
        {
            return new Point(p.X, p.Y);
        }
        public static implicit operator Vector2(Position p)
        {
            return new Vector2(p.X, p.Y);
        }
    }
}
