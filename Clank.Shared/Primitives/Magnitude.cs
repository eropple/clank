﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.Primitives
{
    public struct Magnitude
    {
        public readonly Single X;
        public readonly Single Y;

        public Magnitude(float x, float y)
        {
            X = x;
            Y = y;
        }
    }
}
