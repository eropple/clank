﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Clank.Content;
using Common.Logging;
using FarseerPhysics.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Clank.Primitives
{
    public class Polygon : IEnumerable<Vector2>
    {
        public readonly IReadOnlyList<Vector2> Vertices;

        public Polygon(IEnumerable<Vector2> vertices)
        {
            Vertices = vertices.ToList();
        }

        public IEnumerator<Vector2> GetEnumerator()
        {
            return Vertices.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Vertices.GetEnumerator();
        }

        public Vertices ToFarseerVertices(Vector2 scaleFactor)
        {
            return new Vertices(Vertices.Select(p => new Vector2(p.X / scaleFactor.X, p.Y / scaleFactor.Y)));
        }
    }

    public class PolygonReader : BaseContentTypeReader<Polygon>
    {
        protected override Polygon DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance)
        {
            Int32 count = input.ReadInt32();
            List<Vector2> v = new List<Vector2>(count);
            for (Int32 i = 0; i < count; ++i) v.Add(input.ReadVector2());
            return new Polygon(v);
        }
    }
}
