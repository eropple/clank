﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Clank.Primitives
{
    public struct Size
    {
        public readonly Int32 Width;
        public readonly Int32 Height;

        public Size(Int32 width, Int32 height)
        {
            Width = width;
            Height = height;
        }

        public static implicit operator Point(Size p)
        {
            return new Point(p.Width, p.Height);
        }
        public static implicit operator Vector2(Size p)
        {
            return new Vector2(p.Width, p.Height);
        }
    }
}
