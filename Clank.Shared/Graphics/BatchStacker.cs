﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Clank.Graphics
{
    public class BatchStacker
    {
        public readonly SpriteBatch Batch;
        private List<Matrix> _matrices = new List<Matrix>();

        private readonly SpriteSortMode _sortMode;
        private readonly BlendState _blendState;
        private readonly SamplerState _samplerState;
        private readonly DepthStencilState _depthStencilState;
        private readonly RasterizerState _rasterizerState;
        private readonly Effect _effect;

        private Boolean _isActive = false;

        public BatchStacker(GraphicsDevice graphicsDevice,
                            SpriteSortMode? sortMode = null, BlendState blendState = null,
                            SamplerState samplerState = null, DepthStencilState depthStencilState = null,
                            RasterizerState rasterizerState = null, Effect effect = null)
        {
            Batch = new SpriteBatch(graphicsDevice);

            _sortMode = sortMode ?? SpriteSortMode.Deferred;
            _blendState = blendState ?? BlendState.AlphaBlend;
            _samplerState = samplerState ?? SamplerState.LinearClamp;
            _depthStencilState = depthStencilState ?? DepthStencilState.None;
            _rasterizerState = rasterizerState ?? RasterizerState.CullCounterClockwise;
            _effect = effect;

            _matrices.Add(Matrix.Identity);
        }

        public void Begin()
        {
            if (_isActive) return;
            Batch.Begin(_sortMode, _blendState, _samplerState, _depthStencilState, _rasterizerState, _effect, _matrices[_matrices.Count - 1]);
            _isActive = true;
        }

        public void End()
        {
            if (!_isActive) return;
            Batch.End();
            _isActive = false;
        }

        public void Push(Matrix matrix)
        {
            if (_isActive) End();
            _matrices.Add(_matrices[_matrices.Count - 1] * matrix);
            Begin();
        }

        public void Push(Single x, Single y)
        {
            Push(Matrix.CreateTranslation(x, y, 0));
        }

        public void Push(Vector2 v)
        {
            Push(Matrix.CreateTranslation(v.X, v.Y, 0));
        }

        public void With(Matrix matrix, Action action)
        {
            Push(matrix);
            action.Invoke();
            Pop();
        }

        public void With(Vector2 v, Action action) { With(Matrix.CreateTranslation(v.X, v.Y, 0), action);}

        public void Pop()
        {
            if (_isActive) End();
            if (_matrices.Count == 1) return;

            _matrices.RemoveAt(_matrices.Count - 1);
            Begin();
        }

        public Matrix Peek()
        {
            return _matrices[_matrices.Count - 1];
        }

        public void Reset()
        {
            if (_isActive) End();
            _matrices.Clear();
            _matrices.Add(Matrix.Identity);
        }

        public void Draw(Texture2D texture, Vector2? position = null, Rectangle? drawRectangle = null,
            Rectangle? sourceRectangle = null, Vector2? origin = null, float rotation = 0, Vector2? scale = null,
            Color? color = null, SpriteEffects effect = SpriteEffects.None, float depth = 0)
        {
            Batch.Draw(texture, position, drawRectangle, sourceRectangle, origin, rotation, scale, color, effect, depth);
        }

        public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin,
            Vector2 scale, SpriteEffects effect, float depth)
        {
            Batch.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, effect, depth);
        }

        public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin,
            float scale, SpriteEffects effect, float depth)
        {
            Batch.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, effect, depth);
        }

        public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color, float rotation,
            Vector2 origin, SpriteEffects effect, float depth)
        {
            Batch.Draw(texture, destinationRectangle, sourceRectangle, color, rotation, origin, effect, depth);
        }

        public void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color)
        {
            Batch.Draw(texture, position, sourceRectangle, color);
        }

        public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color)
        {
            Batch.Draw(texture, destinationRectangle, sourceRectangle, color);
        }

        public void Draw(Texture2D texture, Vector2 position, Color color)
        {
            Batch.Draw(texture, position, color);
        }

        public void Draw(Texture2D texture, Rectangle rectangle, Color color)
        {
            Batch.Draw(texture, rectangle, color);
        }

        public static implicit operator SpriteBatch(BatchStacker s)
        {
            return s.Batch;
        }
    }
}
