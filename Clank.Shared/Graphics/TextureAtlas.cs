﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Clank.Content;
using Clank.Graphics.Sources;
using Common.Logging;
using Microsoft.Xna.Framework.Content;

namespace Clank.Graphics
{
    public class TextureAtlas
    {
        public readonly TextureSource Source;
        public readonly ReadOnlyDictionary<String, TextureRegionSource> Entries;

        public TextureAtlas(TextureSource source, Dictionary<String, TextureRegionSource> entries)
        {
            Source = source;
            Entries = new ReadOnlyDictionary<String, TextureRegionSource>(new Dictionary<String, TextureRegionSource>(entries));
        }

        public TextureRegionSource this[String key]
        {
            get
            {
                TextureRegionSource region;
                return Entries.TryGetValue(key, out region) ? region : null;
            }
        }
    }

    public class TextureAtlasReader : BaseContentTypeReader<TextureAtlas>
    {
        protected override TextureAtlas DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance)
        {
            TextureSource source = input.ReadObject<TextureSource>();
            Int32 count = input.ReadInt32();
            var entries = new Dictionary<String, TextureRegionSource>(count);

            for (Int32 i = 0; i < count; ++i)
            {
                entries.Add(input.ReadString(), new TextureRegionSource(source.Info.Texture, input.ReadRectangle()));
            }

            return new TextureAtlas(source, entries);
        }
    }
}
