﻿using Clank.Graphics.Sources;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clank.Primitives;
using Clank.Utils;
using Tracksuit.Graphics.Fonts.Metrics;

namespace Clank.Graphics.Fonts
{
    public enum HorizontalAlign
    {
        Left,
        Center,
        Right
    }

    public enum VerticalAlign
    {
        Top,
        Center,
        Bottom
    }
    public class TextSource
    {
        public readonly BitmapFont Font;
        public readonly Int32 WrapWidth;
        private readonly List<List<GlyphInfo>> _glyphs;
        private readonly List<Int32> _glyphLineLengths;

        public readonly IReadOnlyList<IReadOnlyList<GlyphInfo>> Glyphs; 
        public Rectangle Bounds => new Rectangle(0, 0,
                                    _glyphLineLengths.Max(),
                                    _glyphs.Count * Font.LineHeight);

        internal TextSource(BitmapFont font, List<List<GlyphInfo>> glyphs, Int32 wrapWidth)
        {
            Font = font;
            _glyphs = glyphs;
            Glyphs = _glyphs.Select(line => line).ToList();
            _glyphLineLengths = _glyphs.Select(line => GetLineWidth(font, line)).ToList();
            WrapWidth = wrapWidth;
        }

        public void Draw(BatchStacker batch, Point offset, Int32 startLine = 0, Single customAlpha = 1.0f,
                            Color? colorOverride = null,
                            HorizontalAlign horizontalAlign = HorizontalAlign.Left,
                            VerticalAlign verticalAlign = VerticalAlign.Top)
        {
            batch.Push(offset.X, offset.Y);
            Draw(batch, startLine, customAlpha, colorOverride, horizontalAlign, verticalAlign);
            batch.Pop();
        }

        public void Draw(BatchStacker batch, Vector2 offset, Int32 startLine = 0, Single customAlpha = 1.0f,
            Color? colorOverride = null,
            HorizontalAlign horizontalAlign = HorizontalAlign.Left,
            VerticalAlign verticalAlign = VerticalAlign.Top)
        {
            Draw(batch, offset.ToPoint(), startLine, customAlpha, colorOverride, horizontalAlign, verticalAlign);
        }

        public void Draw(BatchStacker batch, Int32 startLine = 0, Single customAlpha = 1.0f,
                         Color? colorOverride = null,
                         HorizontalAlign horizontalAlign = HorizontalAlign.Left,
                         VerticalAlign verticalAlign = VerticalAlign.Top)
        {
            if (Font.IsDisposed)
                throw new ClankException("TextSource has outlived its BitmapFont.");

            float yStart = 0;
            switch (verticalAlign)
            {
                case VerticalAlign.Center:
                    yStart = -(Bounds.Height/2);
                    break;
                case VerticalAlign.Bottom:
                    yStart = -Bounds.Height;
                    break;
            }

            for (Int32 lineNumber = startLine; lineNumber < _glyphs.Count; ++lineNumber)
            {
                float y = yStart + lineNumber * Font.LineHeight + 0;
                float xpos = 0;
                switch (horizontalAlign)
                {
                    case HorizontalAlign.Center:
                        xpos = -(_glyphLineLengths[lineNumber] / 2);
                        break;
                    case HorizontalAlign.Right:
                        xpos = -_glyphLineLengths[lineNumber];
                        break;
                }

                List<GlyphInfo> line = _glyphs[lineNumber];

                for (Int32 character = 0; character < line.Count; ++character)
                {
                    GlyphInfo glyph = line[character];
                    Int32 kern = 0;
                    if (glyph.Character != ' ')
                    {
                        DrawInfo draw = glyph.CharacterInfo.Region.GetDrawInfo(0);
                        if (character > 0)
                            kern = Font.GetKerningValue(Convert.ToInt32(line[character - 1].Character), Convert.ToInt32(line[character].Character)) ?? 0;

                        Vector2 position = new Vector2(xpos + kern + glyph.CharacterInfo.Offset.X, y + glyph.CharacterInfo.Offset.Y);

                        var color = colorOverride ?? glyph.Color;
                        color.A = (Byte) (255 * customAlpha);
                        batch.Draw(draw.Texture, position, draw.Rectangle, color);
                    }

                    xpos += glyph.CharacterInfo.XAdvance + kern;
                }
            }
        }

        private static Int32 GetLineWidth(BitmapFont font, List<GlyphInfo> line)
        {
            var width = 0;

            for (var character = 0; character < line.Count; ++character)
            {
                var glyph = line[character];
                var kern = 0;
                if (glyph.Character != ' ')
                {
                    if (character > 0)
                        kern = font.GetKerningValue(Convert.ToInt32(line[character - 1].Character), Convert.ToInt32(line[character].Character)) ?? 0;
                }

                width += glyph.CharacterInfo.XAdvance + kern;
            }

            return width;
        }



        public class GlyphInfo
        {
            public readonly Char Character;
            public readonly CharacterInfo CharacterInfo;
            public readonly Color Color;

            public GlyphInfo(Char c, CharacterInfo info, Color color)
            {
                this.Character = c;
                this.CharacterInfo = info;
                this.Color = color;
            }

            public override string ToString()
            {
                return Character.ToString() + " " + Color.ToString();
            }
        }
    }
}
