﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GlyphInfo = Clank.Graphics.Fonts.TextSource.GlyphInfo;
using Tracksuit.Graphics.Fonts.Metrics;
using Clank.Graphics.Sources;
using Clank.Content;
using Clank.Primitives;
using Clank.Utils;
using Common.Logging;

namespace Clank.Graphics.Fonts
{
    /// <summary>
    /// Translation object to turn the BMFont stuff into a somewhat
    /// sane object format for our use.
    /// </summary>
    public class BitmapFont : IExtendedDisposable
    {
        public const String DebugFontName = "Fonts/DebugFont.font-xml";

        /**
            Color code guide (remember, colors are parsed on a stack):

            {#RRGGBB} - push (RGB, alpha 255) onto the stack.
            {#RRGGBBAA} - push RGBA onto the stack.

            {@+N} - push the current color, lightened by N * 10%, onto the stack.
            {@-N} - push the current color, darkened by N * 10%, onto the stack.

            {@D} - push default color onto the stack.
            {@P} - pop color off the stack.
            {@R} - clear stack, resetting to default color.
         */
        protected static readonly Regex ColorCodeRegex =
            new Regex(@"^\{
                          (?:
                            (?:
                              (?<Mode>[\#])
                              (?<Red>[a-fA-F0-9]{2})
                              (?<Green>[a-fA-F0-9]{2})
                              (?<Blue>[a-fA-F0-9]{2})
                              (?<Alpha>[a-fA-F0-9]{2})?
                            )
                            |
                            (?:
				              (?<Mode>[\@])
                              (?:
                                (?:
                                  (?<ExMode>[\-\+])
                                  (?<Magnitude>[0-9])
                                )
                                |
                                (?:
                                  (?<ExMode>[DPR])
                                )
                              )
                            )
                          )
                        \}", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

        public readonly String Face;

        public readonly Int32 LineHeight;
        public readonly Int32 Baseline;
        public readonly ReadOnlyDictionary<Int32, TextureSource> Pages;

        public readonly ReadOnlyDictionary<Int32, CharacterInfo> CharacterTable;

        public readonly ReadOnlyDictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>> KerningTable;

        public Color DefaultColor { get; set; }

        public BitmapFont(String face, Int32 lineHeight, Int32 baseline, 
                        Dictionary<Int32, TextureSource> pages, List<CharacterInfo> chars,
                        List<KerningInfo> kernings)
        {
            this.DefaultColor = Color.White;

            this.Face = face;

            this.LineHeight = lineHeight;
            this.Baseline = baseline;

            this.Pages = new ReadOnlyDictionary<Int32, TextureSource>(pages);

            Dictionary<Int32, CharacterInfo> charDict = new Dictionary<int,CharacterInfo>(chars.Count);
            foreach (var c in chars) charDict.Add(c.ID, c);
            this.CharacterTable = new ReadOnlyDictionary<Int32, CharacterInfo>(charDict);

            Dictionary<Int32, Dictionary<Int32, KerningInfo>> kernDict = new Dictionary<Int32, Dictionary<Int32, KerningInfo>>();
            foreach (KerningInfo k in kernings)
            {
                Dictionary<Int32, KerningInfo> subDict = null;

                if (kernDict.TryGetValue(k.First, out subDict) == false)
                {
                    subDict = new Dictionary<Int32, KerningInfo>();
                    kernDict.Add(k.First, subDict);
                }
                
                subDict.Add(k.Second, k);
            }
            Dictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>> roDict = new Dictionary<Int32,ReadOnlyDictionary<Int32,KerningInfo>>();
            foreach (Int32 k in kernDict.Keys)
            {
                roDict.Add(k, new ReadOnlyDictionary<Int32, KerningInfo>(kernDict[k]));
            }
            this.KerningTable = new ReadOnlyDictionary<Int32, ReadOnlyDictionary<Int32, KerningInfo>>(roDict);
        }

        public Int32? GetKerningValue(Int32 a, Int32 b)
        {
            ReadOnlyDictionary<Int32, KerningInfo> subDict = null;

            if (this.KerningTable.TryGetValue(a, out subDict) == false)
            {
                return null;
            }

            KerningInfo k = null;

            if (subDict.TryGetValue(b, out k) == false)
            {
                return null;
            }

            return k.Amount;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            if (IsDisposed) return;
            foreach (TextureSource s in Pages.Values) s.Dispose();
            IsDisposed = true;
        }


        public TextSource BuildTextSource(String input, Color defaultColor, Int32 wrapWidth = -1)
        {
            return BuildTextSource(input.Replace("\r\n", "\n").Split('\n'), defaultColor, wrapWidth);
        }
        public TextSource BuildTextSource(IEnumerable<String> input, Color defaultColor, Int32 wrapWidth = -1)
        {
            // TODO: support \n
            var colorStack = new Stack<Color>(5);

            List<List<GlyphInfo>> lines = new List<List<GlyphInfo>>();
            colorStack.Push(defaultColor);

            foreach (var text in input)
            {
                StringBuilder work = new StringBuilder(text);

                List<GlyphInfo> line = new List<GlyphInfo>();
                Int32 lineLength = 0;
                List<GlyphInfo> word = new List<GlyphInfo>();
                Int32 wordLength = 0;

                while (work.Length > 0)
                {
                    var chopLength = ControlCheck(work.ToString(), defaultColor, colorStack);
                    if (chopLength > 0)
                    {
                        work.Remove(0, chopLength);
                        continue; // breaks to top of while
                    }
                    if (colorStack.Count == 0) colorStack.Push(defaultColor);

                    Char c = work[0];
                    if (c == '\n')
                    {
                        line.AddRange(word);
                        lineLength += wordLength;

                        lines.Add(line); // old line is archived

                        line = new List<GlyphInfo>(); // word becomes the new line (start of new line)
                        lineLength = 0; // carry over the value
                        word = new List<GlyphInfo>();
                        wordLength = 0;
                    }
                    else
                    {
                        CharacterInfo charInfo = this.CharacterTable[Convert.ToInt32(c)];


                        word.Add(new GlyphInfo(c, charInfo, colorStack.Peek()));
                        wordLength += charInfo.XAdvance;

                        if (c == ' ')
                        {
                            if (wrapWidth != -1) // word wrapping is on
                            {
                                if (wordLength + lineLength > wrapWidth)
                                {
                                    lines.Add(line); // old line is archived

                                    line = word; // word becomes the new line (start of new line)
                                    lineLength = wordLength; // carry over the value
                                }
                                else
                                {
                                    line.AddRange(word);
                                    lineLength += wordLength;
                                }

                                word = new List<GlyphInfo>(); // reset for next word
                                wordLength = 0;
                            }
                            else
                            {
                                // no word wrapping, just add to list
                                line.AddRange(word);
                                lineLength += wordLength;

                                word = new List<GlyphInfo>(); // reset for next word
                                wordLength = 0;
                            }
                        }
                    }

                    work.Remove(0, 1);
                }

                if (wrapWidth != -1) // word wrapping is on
                {
                    if (wordLength + lineLength > wrapWidth)
                    {
                        lines.Add(line); // old line is archived

                        line = word; // word becomes the new line (start of new line)
                        lineLength = wordLength; // carry over the value
                    }
                    else
                    {
                        line.AddRange(word);
                        lineLength += wordLength;
                    }

                    word = new List<GlyphInfo>(); // reset for next word
                    wordLength = 0;
                }
                else
                {
                    line.AddRange(word);
                    lineLength += wordLength;
                }

                lines.Add(line);
            }

            return new TextSource(this, lines, wrapWidth);
        }

        protected static Int32 ControlCheck(String work, Color defaultColor, Stack<Color> colorStack)
        {
            var ccr = ColorCodeRegex.Match(work);
            if (ccr.Success)
            {
                if (ccr.Groups["Mode"].Value == "#")
                {
                    var color = Color.FromNonPremultiplied(Byte.Parse(ccr.Groups["Red"].Value, NumberStyles.HexNumber),
                                                            Byte.Parse(ccr.Groups["Green"].Value, NumberStyles.HexNumber),
                                                            Byte.Parse(ccr.Groups["Blue"].Value, NumberStyles.HexNumber),
                                                            ccr.Groups["Alpha"].Success
                                                                ? Byte.Parse(ccr.Groups["Alpha"].Value, NumberStyles.HexNumber)
                                                                : Byte.MaxValue);
                    colorStack.Push(color);
                }
                else if (ccr.Groups["Mode"].Value == "@")
                {
                    switch (ccr.Groups["ExMode"].Value)
                    {
                        case "+":
                            colorStack.Push(colorStack.Peek().Lighten(Single.Parse(ccr.Groups["Magnitude"].Value) / 10.0f));
                            break;
                        case "-":
                            colorStack.Push(colorStack.Peek().Darken(Single.Parse(ccr.Groups["Magnitude"].Value) / 10.0f));
                            break;
                        case "D":
                            colorStack.Push(defaultColor);
                            break;
                        case "P":
                            if (colorStack.Count > 0)
                            {
                                colorStack.Pop();
                            }
                            else
                            {
                                colorStack.Push(defaultColor);
                            }
                            break;
                        case "R":
                            colorStack.Clear();
                            colorStack.Push(defaultColor);
                            break;
                        default:
                            throw new TextParseException("Unrecognized escape exmode: '{0}'", ccr.Groups["ExMode"].Value);
                    }
                }
                else
                {
                    throw new TextParseException("Unrecognized escape mode: '{0}'", ccr.Groups["Mode"].Value);
                }
                return ccr.Length;
            }

            return 0;
        }





        public class TextParseException : ClankException
        {
            public TextParseException()
            {
            }

            public TextParseException(string message) : base(message)
            {
            }

            public TextParseException(string message, Exception innerException) : base(message, innerException)
            {
            }

            public TextParseException(Exception innerException, string message, params object[] args) : base(innerException, message, args)
            {
            }

            public TextParseException(string message, params object[] args) : base(message, args)
            {
            }
        }
    }

    public class BitmapFontReader : BaseContentTypeReader<BitmapFont>
    {
        protected override BitmapFont DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance)
        {
            String face = input.ReadString();
            Int32 lineHeight = input.ReadInt32();
            Int32 baseline = input.ReadInt32();

            Int32 pageCount = input.ReadInt32();
            Dictionary<Int32, TextureSource> pages = new Dictionary<int, TextureSource>(pageCount);
            for (Int32 i = 0; i < pageCount; ++i)
            {
                pages.Add(input.ReadInt32(), input.ReadObject<TextureSource>());
            }

            Int32 charCount = input.ReadInt32();
            List<CharacterInfo> chars = new List<CharacterInfo>(charCount);
            for (Int32 i = 0; i < charCount; ++i)
            {
                chars.Add(new CharacterInfo(input.ReadInt32(), new Point(input.ReadInt32(), input.ReadInt32()),
                    new Point(input.ReadInt32(), input.ReadInt32()),
                    new Point(input.ReadInt32(), input.ReadInt32()), input.ReadInt32(), pages[input.ReadInt32()]));
            }

            Int32 kernCount = input.ReadInt32();
            List<KerningInfo> kerns = new List<KerningInfo>(kernCount);
            for (Int32 i = 0; i < kernCount; ++i)
            {
                kerns.Add(new KerningInfo(input.ReadInt32(), input.ReadInt32(), input.ReadInt32()));
            }

            return new BitmapFont(face, lineHeight, baseline, pages, chars, kerns);
        }
    }
}
