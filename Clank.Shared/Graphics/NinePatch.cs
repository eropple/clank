﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Clank.Content;
using Clank.Graphics.Sources;
using Clank.Primitives;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Clank.Graphics
{
    public class NinePatch
    {
        public Size MinimumSize { get; private set; }

        public NinePatch(IDrawSource topLeft, IDrawSource topMiddle, IDrawSource topRight,
            IDrawSource left, IDrawSource middle, IDrawSource right,
            IDrawSource bottomLeft, IDrawSource bottomMiddle, IDrawSource bottomRight)
        {
            TopLeft = topLeft;
            TopMiddle = topMiddle;
            TopRight = topRight;

            Left = left;
            Middle = middle;
            Right = right;

            BottomLeft = bottomLeft;
            BottomMiddle = bottomMiddle;
            BottomRight = bottomRight;

            MinimumSize = new Size(TopLeft.GetDrawInfo().Rectangle.Width +
                                   TopMiddle.GetDrawInfo().Rectangle.Width +
                                   TopRight.GetDrawInfo().Rectangle.Width,
                                   TopLeft.GetDrawInfo().Rectangle.Height +
                                   Left.GetDrawInfo().Rectangle.Height +
                                   BottomLeft.GetDrawInfo().Rectangle.Height);
        }
        public NinePatch(TextureAtlas atlas) : this(String.Empty, atlas) { }
        public NinePatch(String prefix, TextureAtlas atlas)
            : this(atlas[prefix + "TopLeft"], atlas[prefix + "TopMiddle"], atlas[prefix + "TopRight"],
                   atlas[prefix + "Left"], atlas[prefix + "Middle"], atlas[prefix + "Right"],
                   atlas[prefix + "BottomLeft"], atlas[prefix + "BottomMiddle"], atlas[prefix + "BottomRight"]) { }

        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public NinePatch(Int32 duration, IEnumerable<String> prefixes, TextureAtlas atlas)
            : this(_A(atlas, duration, prefixes, "TopLeft"),
                   _A(atlas, duration, prefixes, "TopMiddle"),
                   _A(atlas, duration, prefixes, "TopRight"),

                   _A(atlas, duration, prefixes, "Left"),
                   _A(atlas, duration, prefixes, "Middle"),
                   _A(atlas, duration, prefixes, "Right"),

                   _A(atlas, duration, prefixes, "BottomLeft"),
                   _A(atlas, duration, prefixes, "BottomMiddle"),
                   _A(atlas, duration, prefixes, "BottomRight")) { }

        public IDrawSource TopLeft { get; }
        public IDrawSource TopMiddle { get; }
        public IDrawSource TopRight { get; }

        public IDrawSource Left { get; }
        public IDrawSource Middle { get; }
        public IDrawSource Right { get; }

        public IDrawSource BottomLeft { get; }
        public IDrawSource BottomMiddle { get; }
        public IDrawSource BottomRight { get; }

        private static SimpleAnimationSource _A(TextureAtlas atlas, Int32 duration, IEnumerable<String> prefixes, String key)
        {
            return new SimpleAnimationSource(duration, prefixes.Select(prefix => atlas.Entries[prefix + key]));
        }
    }
}
