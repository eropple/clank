﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Clank.Graphics.Sources
{
    public class TextureRegionSource : IDrawSource
    {
        protected readonly DrawInfo Info;

        public TextureRegionSource(Texture2D texture, Rectangle rectangle)
        {
            Info = new DrawInfo(texture, rectangle);
        }

        public DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = 0)
        {
            return Info;
        }
    }
}
