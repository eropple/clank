﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Clank.Graphics.Sources
{
    public interface IDrawSource
    {
        DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = DrawSourceOptions.None);
    }

    public struct DrawInfo
    {
        public readonly Texture2D Texture;
        public readonly Rectangle Rectangle;

        public DrawInfo(Texture2D texture, Rectangle rectangle)
        {
            Texture = texture;
            Rectangle = rectangle;
        }
    }

    [Flags]
    public enum DrawSourceOptions
    {
        None = 0,
        AnimationClamps
    }
}
