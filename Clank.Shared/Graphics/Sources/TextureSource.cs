﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Clank.Content;
using Common.Logging;

namespace Clank.Graphics.Sources
{
    public class TextureSource : IDrawSource, IDisposable
    {
        public readonly DrawInfo Info;

        internal TextureSource(Texture2D texture)
        {
            this.Info = new DrawInfo(texture, texture.Bounds);
        }

        public DrawInfo GetDrawInfo(Int64 time = 0, DrawSourceOptions options = 0)
        {
            return Info;
        }

        public void Dispose()
        {
            Info.Texture.Dispose();
        }

        public TextureRegionSource CreateRegionSource(Rectangle rectangle)
        {
            Rectangle r = rectangle;
            r.Offset(Info.Rectangle.Location);
            // TODO: bounds checking (keep inside parent rect)
            return new TextureRegionSource(Info.Texture, r);
        }
    }

    public class TextureSourceReader : BaseContentTypeReader<TextureSource>
    {
        protected override TextureSource DoRead(ContentReader input, ClankContentManager contentManager, object existingInstance)
        {
            return new TextureSource(input.ReadObject<Texture>() as Texture2D);
        }
    }
}
