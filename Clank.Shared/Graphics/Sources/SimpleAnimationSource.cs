﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.Graphics.Sources
{
    public class SimpleAnimationSource : IDrawSource
    {
        public Int32 Duration { get; private set; }
        private readonly List<TextureRegionSource> _frames;

        public SimpleAnimationSource(Int32 duration, IEnumerable<TextureRegionSource> frames)
        {
            Duration = duration;
            _frames = new List<TextureRegionSource>(frames);
        }

        public DrawInfo GetDrawInfo(Int64 time, DrawSourceOptions options = 0)
        {
            Int32 frame = (options & DrawSourceOptions.AnimationClamps) != DrawSourceOptions.AnimationClamps
                ? (Int32)(time % (_frames.Count * Duration)) / Duration
                : (Int32)(Math.Max(time, _frames.Count * Duration) / Duration);

            return _frames[frame % _frames.Count].GetDrawInfo(0, 0);
        }
    }
}
