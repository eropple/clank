﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Clank.Content;
using Clank.Extensibility;
using Clank.GUI;
using Microsoft.Xna.Framework.Graphics;

namespace Clank.ControlFlow.SpecialStates
{
    public abstract class PreloadState : State
    {
        private readonly bool _shouldLoadMods;
        private ModManager _modManager;
        private IEnumerator<LoadRecord> _loadEnumerator;

        private readonly Stopwatch _stopwatch;

        private Int32 _transitionState = 0;
        private Int32 _loadCounter = 0;

        private volatile Boolean _modsLoaded = false;

        protected abstract State NextState { get; }
        protected abstract IReadOnlyList<LoadRecord> LoadRecords { get; }

        protected abstract ClankContentManager PreloadingContentManager{ get; }

        protected String LastLoaded { get; private set; }
        protected PreloadState(Boolean shouldLoadMods = false)
        {
            _shouldLoadMods = shouldLoadMods;
            _stopwatch = new Stopwatch();
        }

        protected override void Dispose(bool disposing) { }

        protected override sealed void Initialize()
        {
            _modManager = PreloadingContentManager.ModManager;
            _loadEnumerator = LoadRecords.Select(record =>
            {
                Logger.Debug(m => m("Loading '{0}' ({1})...", record.AssetName, record.Type.Name));
                record.PreloadFromRecord(PreloadingContentManager);
                Logger.Debug(m => m("'{0}' loaded.", record.AssetName));
                LastLoaded = record.AssetName;
                return record;
            }).GetEnumerator();   

            Logger.DebugFormat("Kicking off mod loader thread.");
            if (_shouldLoadMods)
            {
                new Thread(LoadMods).Start();
            }
        }

        protected override void Update(UpdateEventArgs args)
        {
            switch (_transitionState)
            {
                case 0:
                {
                    if (UpdateStart(args))
                    {
                        Logger.DebugFormat("UpdateStart done, transitioning to asset loader.");
                        _transitionState = 1;
                    }
                    break;
                }
                case 1:
                {
                    // Load content assets for 10ms or until done.
                    _stopwatch.Restart();
                    while (_stopwatch.ElapsedMilliseconds < 10)
                    {
                        // more content to load, so do it
                        if (_loadEnumerator.MoveNext()) continue;

                        // we've hit the end of the list, but mods may not be loaded
                        if (!_modsLoaded && _shouldLoadMods) return;

                        // mods are loaded, let's go.
                        Logger.DebugFormat("Asset loader and mod loader done, transitioning to UpdateEnd.");
                        _transitionState = 2;
                        break;
                    }
                    break;
                }
                case 2:
                {
                    if (UpdateEnd(args))
                    {
                        Logger.DebugFormat("UpdateEnd done, launching new state.");
                        StateManager.Pop();
                        StateManager.Push(NextState);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Called on every invocation of Update() until it returns true, at which point
        /// it begins loading items (state 1).
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="debug"></param>
        /// <returns>True when whatever setup (visual fade-in, etc.) is done, false otherwise.</returns>
        protected virtual Boolean UpdateStart(UpdateEventArgs args) { return true; }

        /// <summary>
        /// After all content items have been loaded and 
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="debug"></param>
        /// <returns>True when whatever setup (visual fade-in, etc.) is done, false otherwise.</returns>
        protected virtual Boolean UpdateEnd(UpdateEventArgs args) { return true; }


        private void LoadMods()
        {
            Logger.DebugFormat("Loading mod extensions...");
            _modManager.LoadExtensions();
            Logger.DebugFormat("Mod extensions loaded.");
            _modsLoaded = true;
        }
    }
}
