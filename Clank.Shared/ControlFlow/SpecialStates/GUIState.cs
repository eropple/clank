﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Graphics;
using Clank.GUI;
using Clank.GUI.Controls;
using Clank.Primitives;
using Microsoft.Xna.Framework.Graphics;

namespace Clank.ControlFlow.SpecialStates
{
    public abstract class GUIState : State
    {
        public StatePanel Panel { get; private set; }

        protected GUIState()
        {
            Panel = new StatePanel();
        }

        protected override void LoadContent()
        {
        }

        protected override void Initialize()
        {
            var config = _game.ClankConfiguration;
            Panel._size = new Size(config.Graphics.WindowWidth, config.Graphics.WindowHeight);

            InitializeComponents();
        }

        protected abstract void InitializeComponents();

        protected override void Update(UpdateEventArgs args)
        {
            BeforeGUIUpdate(args);
            Panel.DoUpdate(Panel, args);
            AfterGUIUpdate(args);
        }
        protected virtual void BeforeGUIUpdate(UpdateEventArgs args) { }
        protected virtual void AfterGUIUpdate(UpdateEventArgs args) { }

        protected override void Draw(BatchStacker batch, UpdateEventArgs args)
        {
            BeforeGUIDraw(batch, args);
            batch.Reset();
            Panel.DoDraw(Panel, batch, args);
            batch.Reset();
            AfterGUIDraw(batch, args);
        }
        protected virtual void BeforeGUIDraw(BatchStacker batch, UpdateEventArgs args) { }
        protected virtual void AfterGUIDraw(BatchStacker batch, UpdateEventArgs args) { }

        public override bool KeyUp(KeyEventArgs eventArgs)
        {
            return Panel.DispatchKeyUp(eventArgs);
        }

        public override bool KeyDown(KeyEventArgs eventArgs)
        {
            return Panel.DispatchKeyDown(eventArgs);
        }

        public override bool MouseMove(MouseMoveEventArgs args)
        {
            return Panel.DispatchMouseMove(args);
        }

        public override bool MouseWheel(MouseWheelEventArgs args)
        {
            return Panel.DispatchMouseWheel(args);
        }

        public override bool MouseUp(MouseButtonEventArgs args)
        {
            return Panel.DispatchMouseUp(args);
        }

        public override bool MouseDown(MouseButtonEventArgs args)
        {
            return Panel.DispatchMouseDown(args);
        }

        public override bool GamePadButtonUp(GamePadButtonEventArgs args)
        {
            return base.GamePadButtonUp(args);
        }

        public override bool GamePadButtonDown(GamePadButtonEventArgs args)
        {
            return base.GamePadButtonDown(args);
        }

        public override bool GamePadThumbstickMove(GamePadThumbstickEventArgs args)
        {
            return base.GamePadThumbstickMove(args);
        }

        public override bool GamePadTriggerMove(GamePadTriggerEventArgs args)
        {
            return base.GamePadTriggerMove(args);
        }

        public sealed class StatePanel : Panel
        {
            private State _state;

            internal Size _size;
            public override Size Size => _size;
        }
    }
}
