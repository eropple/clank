﻿using Clank.Graphics.Fonts;
using Clank.Input;
using Clank.Utility;
using Common.Logging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Clank.Content;
using Clank.Graphics;
using Clank.GUI;
using Clank.Text;

namespace Clank.ControlFlow
{
    public class StateManager
    {
        private readonly ILog Logger;

        private readonly List<State> _states;
        private readonly HashSet<State> _killSet;

        public readonly GameBase Game;

        private Boolean _stateStackChanged = false;
#if DEBUG
        private Boolean _debug = true;
#else
        private Boolean _debug = false;
#endif

        private BatchStacker _batch = null;
        private BitmapFont _debugFont;

        public StateManager(GameBase game)
        {
            Game = game;
            Logger = Game.Logging.GetLogger<StateManager>();

            _states = new List<State>(20);
            _killSet = new HashSet<State>();
        }

        public State TopState {  get { return _states.Count > 0 ? _states.Last() : null; } }

        public void Push(State newState)
        {
            State old = _states.Count > 0 ? _states[_states.Count - 1] : null;

            Logger.Info(m => m("Pushing {0}.", newState));

            newState.DoLoadContent(Game);
            _states.Add(newState);
            newState.DoInitialize();

            _killSet.Remove(newState);

            if (old != null)
                old.NoLongerTopState(newState);
        }

        public State Pop()
        {
            State old = _states.Count > 0 ? _states[_states.Count - 1] : null;

            if (old == null)
                throw new Exception("Tried to pop with no states.");

            State newTop = _states.Count > 1 ? _states[_states.Count - 2] : null;

            Logger.Info(m => m("Popping {0} over {1}.", newTop, StringUtils.toString(old)));

            _states.RemoveAt(_states.Count - 1);

            if (newTop != null)
                newTop.BecameTopState(old);

            _killSet.Add(old);

            return old;
        }

        public Boolean Remove(State state)
        {
            State top = _states.Count > 0 ? _states[_states.Count - 1] : null;
            if (state == top)
            {
                Pop();
                return true;
            }

            if (_states.Remove(state))
            {
                _stateStackChanged = true;
                Logger.Warn(m => m("Removed {0} from the state stack (not on top)."));
                return true;
            }
            Logger.Warn(m => m("Attempted to remove {0}, but it's not in the state stack.", state));
            return false;
        }


        public void Update(GameTime gameTime)
        {
            if (_states.Count == 0)
                throw new Exception("Cannot call Update() with a state count of zero.");

            _stateStackChanged = false;

            Int32 idx = _states.FindLastIndex((State obj) => obj.SoaksUpdates);
            if (idx == -1)
                idx = 0;



            State current = null;

            Int64 delta = (Int64) Math.Round(gameTime.ElapsedGameTime.TotalMilliseconds);

            restartLoop:
            for (Int32 j = idx; j < _states.Count; ++j)
            {
                current = _states[j];

                current.DoUpdate(new UpdateEventArgs(delta, j == _states.Count - 1, _debug));

                // So this is kind of tough. If the state stack has changed, we need to find the next one and resume.
                // What makes this particularly aggravating is that Mono only does the list length for the for loop
                // once and caches the result, so we have to break the loop and try again.
                if (!_stateStackChanged) continue;

                if (_killSet.Contains(current) || _states[j] != current) // in the weird case of a remove+push
                {
                    idx = j; // we don't increment the counter, so this will retry at the same index
                    _stateStackChanged = false;
                    goto restartLoop;
                }
                else if (!_killSet.Contains(current))
                {
                    idx = j + 1; // we need to increment the counter
                    _stateStackChanged = false;
                    goto restartLoop;
                }
            }

            foreach (State s in _killSet)
                s.Dispose();

            _stateStackChanged = false;
        }

        public void Draw(GameTime gameTime)
        {
            Game.GraphicsDevice.Clear(Color.Black);

            if (_batch == null)
            {
                _batch = new BatchStacker(Game.GraphicsDevice);
            }

            Int32 idx = _states.FindLastIndex((State obj) => obj.SoaksDraws);
            if (idx == -1)
                idx = 0;

            Int64 delta = (Int64) Math.Round(gameTime.ElapsedGameTime.TotalMilliseconds);
            for (Int32 j = idx; j < _states.Count; ++j)
            {
                if (_stateStackChanged)
                    throw new Exception("Cannot modify the state stack during drawing.");

                _states[j].DoDraw(_batch, new UpdateEventArgs(delta, j == _states.Count - 1, _debug));
            }

            if (_debug)
            {
                DrawGlobalOverlay();
            }
        }

        private void DrawGlobalOverlay()
        {
            if (_debugFont == null)
            {
                _debugFont = Game.GlobalContentManager.Load<BitmapFont>(BitmapFont.DebugFontName);
            }

            _batch.Push(Matrix.CreateTranslation(2, 2, 0));

            _debugFont.BuildTextSource(String.Format("{1} states: {0}", _states.Last(), _states.Count), Color.White)
                .Draw(_batch);

            _batch.Pop();
        }



        public void KeyUp(KeyEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].KeyUp(args) || _states[i].SoaksInput)
                    return;
        }

        public void KeyDown(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Keys.F6:
                    Logger.Info(m => m("Attempting to toggle full screen."));
                    Game.GraphicsDeviceManager.ToggleFullScreen();
                    break;
                case Keys.F8:
                    Logger.Info(m => m("Toggling debug information."));
                    _debug = !_debug;
                    break;
#if DEBUG
                case Keys.Back: // actually backspace
                    if (args.Control && args.Shift) Environment.Exit(0);
                    break;
#endif
            }
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].KeyDown(args) || _states[i].SoaksInput)
                    return;
        }

        public void MouseMove(MouseMoveEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseMove(args) || _states[i].SoaksInput)
                    return;
        }

        public void MouseWheel(MouseWheelEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseWheel(args) || _states[i].SoaksInput)
                    return;
        }

        public void MouseUp(MouseButtonEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseUp(args) || _states[i].SoaksInput)
                    return;
        }

        public void MouseDown(MouseButtonEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].MouseDown(args) || _states[i].SoaksInput)
                    return;
        }

        public void GamePadButtonUp(GamePadButtonEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadButtonUp(args) || _states[i].SoaksInput)
                    return;
        }

        public void GamePadButtonDown(GamePadButtonEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadButtonDown(args) || _states[i].SoaksInput)
                    return;
        }

        public void GamePadThumbstickMove(GamePadThumbstickEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadThumbstickMove(args) || _states[i].SoaksInput)
                    return;
        }

        public void GamePadTriggerMove(GamePadTriggerEventArgs args)
        {
            for (Int32 i = _states.Count - 1; i >= 0; --i)
                if (_states[i].GamePadTriggerMove(args) || _states[i].SoaksInput)
                    return;
        }
    }
}