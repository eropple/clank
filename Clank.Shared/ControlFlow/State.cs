﻿using Clank.Content;
using Clank.Input;
using Common.Logging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Clank.Graphics;
using Clank.Graphics.Fonts;
using Clank.GUI;
using Clank.Text;
using Unglide;

namespace Clank.ControlFlow
{
    public abstract class State : EasyDisposable
    {
        protected ILog Logger { get; private set; }

        internal GameBase _game;

        public readonly Int32 ID;
        public readonly Tweener Tweener = new Tweener();
        protected ClankContentManager GlobalContent => _game.GlobalContentManager;
        protected StateManager StateManager => _game.StateManager;
        protected GraphicsDeviceManager GraphicsDeviceManager => _game.GraphicsDeviceManager;
        public I18NStore I18N { get; protected set; }
        protected ClankContentManager LocalContent { get; private set; }

        protected BitmapFont DebugFont { get; private set; }

        public Boolean IsInitialized { get; private set; }
        public Boolean IsContentLoaded { get; private set; }

        public Boolean SoaksUpdates { get; protected set; }
        public Boolean SoaksDraws { get; protected set; }
        public Boolean SoaksInput { get; protected set; }

        protected readonly AnchorHolder Anchors;

        protected State(ClankContentManager localContent = null)
        {
            ID = GetIDAndIncrement();

            IsInitialized = false;

            SoaksUpdates = false;
            SoaksDraws = false;
            SoaksInput = false;

            Anchors = new AnchorHolder(this);
            LocalContent = localContent;
        }


        internal void DoLoadContent(GameBase game)
        {
            if (IsContentLoaded)
                return;

            _game = game;
            Logger = game.Logging.GetLogger(GetType());

            DebugFont = _game.GlobalContentManager.Load<BitmapFont>(BitmapFont.DebugFontName);

            if (LocalContent == null)
            {
                LocalContent = new ClankContentManager(GlobalContent);
            }

            I18N = _game.GlobalI18NStore;

            Logger.Debug(m => m("Loading content for #{0}.", ID));
            LoadContent();
            IsContentLoaded = true;
        }

        internal void DoInitialize()
        {
            if (IsInitialized)
                return;

            Logger.Debug(m => m("Initializing #{0}.", ID));
            Initialize();
            IsInitialized = true;
        }
        protected abstract void LoadContent();
        protected abstract void Initialize();

        internal void DoUpdate(UpdateEventArgs args)
        {
            Tweener.Update(args.Delta/ 1000.0f);
            Update(args);
        }
        protected abstract void Update(UpdateEventArgs args);

        internal void DoDraw(BatchStacker batch, UpdateEventArgs args)
        {
            Draw(batch, args);
        }
        protected abstract void Draw(BatchStacker batch, UpdateEventArgs args);


        public virtual void BecameTopState(State oldTopState) { }
        public virtual void NoLongerTopState(State newTopState) { }

#if DESKTOP
        internal Boolean DoClose()
        {
            return CloseRequested();
        }
        /// <returns>True if Clank should quit after running this method. False otherwise.</returns>
        protected virtual Boolean CloseRequested() { return false;  }
#endif
        public virtual Boolean KeyUp(KeyEventArgs eventArgs) { return false; }
        public virtual Boolean KeyDown(KeyEventArgs eventArgs) { return false; }

        public virtual Boolean MouseMove(MouseMoveEventArgs args) { return false; }
        public virtual Boolean MouseWheel(MouseWheelEventArgs args) { return false; }
        public virtual Boolean MouseUp(MouseButtonEventArgs args) { return false; }
        public virtual Boolean MouseDown(MouseButtonEventArgs args) { return false; }

        public virtual Boolean GamePadButtonUp(GamePadButtonEventArgs args) { return false; }
        public virtual Boolean GamePadButtonDown(GamePadButtonEventArgs args) { return false; }
        public virtual Boolean GamePadThumbstickMove(GamePadThumbstickEventArgs args) { return false; }
        public virtual Boolean GamePadTriggerMove(GamePadTriggerEventArgs args) { return false; }

        public override string ToString()
        {
            return String.Format("{0} (#{1})", this.GetType().Name, ID);
        }





        private static readonly Object _managerIdLock = new Object();
        private static Int32 _managerId = 0;

        private static Int32 GetIDAndIncrement()
        {
            lock (_managerIdLock)
            {
                return _managerId++;
            }
        }


        public class AnchorHolder
        {
            private readonly State _state;

            internal AnchorHolder(State state)
            {
                _state = state;
            }

            public Point TopLeft => new Point(0, 0);
            public Point TopCenter => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth / 2, 0);
            public Point TopRight => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth, 0);
            public Point MiddleLeft => new Point(0, _state.GraphicsDeviceManager.PreferredBackBufferHeight / 2);
            public Point MiddleCenter => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth / 2,
                                                   _state.GraphicsDeviceManager.PreferredBackBufferHeight / 2);
            public Point MiddleRight => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth,
                                                   _state.GraphicsDeviceManager.PreferredBackBufferHeight / 2);
            public Point BottomLeft => new Point(0,
                                                 _state.GraphicsDeviceManager.PreferredBackBufferHeight);
            public Point BottomCenter => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth / 2,
                                                   _state.GraphicsDeviceManager.PreferredBackBufferHeight);
            public Point BottomRight => new Point(_state.GraphicsDeviceManager.PreferredBackBufferWidth,
                                                  _state.GraphicsDeviceManager.PreferredBackBufferHeight);
        }
    }
}
