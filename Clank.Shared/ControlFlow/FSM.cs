﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.ControlFlow
{
    public class FSM<TStateType>
    {
        private Dictionary<TStateType, FSMNode> _nodes = new Dictionary<TStateType, FSMNode>(); 
        public FSM()
        {
            var t = typeof(TStateType);
            if (t.IsEnum)
            {
                foreach (var value in Enum.GetValues(t))
                {
                    var v = (TStateType) value;
                    _nodes[v] = new FSMNode(v);
                }
            }
        }

        public FSMNode Register(TStateType key)
        {
            FSMNode node;
            if (!_nodes.TryGetValue(key, out node))
            {
                node = new FSMNode(key);
                _nodes.Add(key, node);
            }
            return node;
        }

        public class FSMNode
        {
            public TStateType Key { get; }

            internal FSMNode(TStateType key)
            {
                Key = key;
            }

            public event Action Enter;
            public event Action Leave;

        }
    }
}
