﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Clank.Utility;
using Clank.Utils;

namespace Clank.Config
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class ClankConfiguration : ConfigBase
    {
        [JsonProperty("Mods")]
        public ModConfiguration Mods { get; private set; }

        [JsonProperty("Graphics")]
        public GraphicsConfiguration Graphics { get; private set; }

        [JsonProperty("Language")]
        public String Language { get; private set; }

#if DESKTOP
        public static ClankConfiguration GetConfiguration()
        {
            var config = Load(Paths.BaseConfigPath);
            var userConfig = Load(Paths.UserConfigPath);

            config.Merge(userConfig,
                         new JsonMergeSettings()
                         {
                             MergeArrayHandling = MergeArrayHandling.Replace
                         });

            using (var jt = new JTokenReader(config))
            {
                var retval = (new JsonSerializer()).Deserialize<ClankConfiguration>(jt);

                if (retval.Graphics == null) throw new ClankException("Graphics clause not found in configuration.");
                if (retval.Mods == null) throw new ClankException("Mods clause not found in configuration.");

                return retval;
            }
        }

        private static JObject Load(String file)
        {
            if (!File.Exists(file)) return new JObject();

            using (var sr = File.OpenText(file))
            using (var jr = new JsonTextReader(sr))
            {
                var obj = JObject.Load(jr);

                var platformObject = obj[StringUtils.PlatformString];
                if (platformObject != null)
                {
                    obj.Merge(platformObject, 
                              new JsonMergeSettings()
                              {
                                  MergeArrayHandling = MergeArrayHandling.Replace
                              });
                }

                return obj;
            }
        }
#else
#error must implement
#endif
    }
}
