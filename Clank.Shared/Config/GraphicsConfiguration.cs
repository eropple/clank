﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.Config
{
    [JsonObject(MemberSerialization.OptIn)]
    public class GraphicsConfiguration : ConfigBase
    {
        [JsonProperty("WindowWidth")]
        public Int32 WindowWidth { get; private set; }
        
        [JsonProperty("WindowHeight")]
        public Int32 WindowHeight { get; private set; }

        [JsonProperty("Fullscreen")]
        public Boolean IsFullscreen { get; private set; }
    }
}
