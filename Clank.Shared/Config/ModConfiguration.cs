﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace Clank.Config
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ModConfiguration : ConfigBase
    {
        [JsonProperty("SelectedMods")]
        public IReadOnlyList<String> SelectedMods { get; private set; }

        [JsonProperty("ContentDirectories")]
        public IReadOnlyList<String> ContentDirectories { get; private set; }

        /// <summary>
        /// A set of assemblies appended to the end of the extension loader list, after all
        /// compiled assemblies from the SelectedMods are readied.
        /// </summary>
        [JsonProperty("AppendedExtensionAssemblies")]
        public IReadOnlyList<String> AppendedExtensionAssemblies { get; private set; }

        /// <summary>
        /// The assemblies that should be used as references for all compiled mods
        /// (typically including the core assembly of the game).
        /// </summary>
        [JsonProperty("UniversalAssemblies")]
        public IReadOnlyList<String> UniversalAssemblies { get; private set; }

        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            ContentDirectories = ContentDirectories.Select(Paths.SubstituteTokens).ToList();
        }
    }
}
