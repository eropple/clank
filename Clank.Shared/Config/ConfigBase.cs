﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Clank.Config
{
    public class ConfigBase
    {
        [JsonExtensionData]
#pragma warning disable 649
        private Dictionary<String, JToken> _additionalData;
#pragma warning restore 649

        public IReadOnlyDictionary<String, JToken> AdditionalData => _additionalData;
    }
}
