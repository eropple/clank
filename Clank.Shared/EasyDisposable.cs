﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank
{
    public abstract class EasyDisposable : IExtendedDisposable
    {
        public void Dispose()
        {
            if (IsDisposed) return;
            Dispose(true);
            GC.SuppressFinalize(this);
            IsDisposed = true;
        }

        /// <summary>
        /// Handles releasing of resources.
        /// </summary>
        /// <param name="disposing">If true, called through IDisposable.Dispose(). If false, called through finalizer.</param>
        protected abstract void Dispose(Boolean disposing);

        public bool IsDisposed { get; private set; }

        ~EasyDisposable()
        {
            Dispose(false);
        }
    }
}
