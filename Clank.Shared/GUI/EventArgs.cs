﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.GUI.Controls;
using Clank.Input;
using Clank.Primitives;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Clank.GUI
{
    public class CancelableArgs<TArgsType>
    {
        public Boolean Cancel { get; set; }
        public readonly TArgsType InnerArgs;

        public CancelableArgs(TArgsType innerArgs)
        {
            InnerArgs = innerArgs;
        }
    }

    public struct FocusEventArgs
    {
        public readonly Control UnfocusedControl;
        public readonly Control FocusedControl;

        public FocusEventArgs(Control unfocusedControl, Control focusedControl)
        {
            UnfocusedControl = unfocusedControl;
            FocusedControl = focusedControl;
        }
    }
    public struct UpdateEventArgs
    {
        public readonly Int64 Delta;
        public readonly Boolean TopOfStack;
        public readonly Boolean Debug;

        public UpdateEventArgs(long delta, bool topOfStack, bool debug)
        {
            Delta = delta;
            TopOfStack = topOfStack;
            Debug = debug;
        }
    }
    public struct ChildControlEventArgs
    {
        public Control Child { get; internal set; }

        public ChildControlEventArgs(Control child)
        {
            Child = child;
        }
    }
    public struct MouseEventArgs
    {
        public Point Position { get; internal set; }

        public MouseEventArgs(Point position)
        {
            Position = position;
        }
    }

    public struct MouseMoveEventArgs
    {
        public Point Position { get; internal set; }
        public Point Delta { get; internal set; }

        public MouseMoveEventArgs(Point position, Point delta)
        {
            Position = position;
            Delta = delta;
        }
    }

    public struct MouseButtonEventArgs
    {
        public Point Position { get; internal set; }
        public MouseButton Button { get; internal set; }
        public Boolean Shift { get; internal set; }
        public Boolean Control { get; internal set; }
        public Boolean Alt { get; internal set; }

        public MouseButtonEventArgs(Point position, MouseButton button, bool shift, bool control, bool alt)
        {
            Position = position;
            Button = button;
            Shift = shift;
            Control = control;
            Alt = alt;
        }
    }

    public struct MouseWheelEventArgs
    {
        public Point Position { get; internal set; }
        public Int32 Direction { get; internal set; }
        public Boolean Shift { get; internal set; }
        public Boolean Control { get; internal set; }
        public Boolean Alt { get; internal set; }

        public MouseWheelEventArgs(Point position, int direction, bool shift, bool control, bool alt)
        {
            Position = position;
            Direction = direction;
            Shift = shift;
            Control = control;
            Alt = alt;
        }
    }

    public struct KeyEventArgs
    {
        public Keys Key { get; internal set; }
        public Boolean Shift { get; internal set; }
        public Boolean Control { get; internal set; }
        public Boolean Alt { get; internal set; }

        public KeyEventArgs(Keys key, bool shift, bool control, bool alt)
        {
            Key = key;
            Shift = shift;
            Control = control;
            Alt = alt;
        }
    }

    public struct GamePadButtonEventArgs
    {
        public PlayerIndex Index { get; internal set; }
        public Buttons Button { get; internal set; }

        public GamePadButtonEventArgs(PlayerIndex index, Buttons button)
        {
            Index = index;
            Button = button;
        }
    }

    public struct GamePadThumbstickEventArgs
    {
        public PlayerIndex Index { get; internal set; }
        public Thumbstick Thumbstick { get; internal set; }
        public Vector2 Position { get; internal set; }
        public Vector2 Delta { get; internal set; }

        public GamePadThumbstickEventArgs(PlayerIndex index, Thumbstick thumbstick, Vector2 position, Vector2 delta)
        {
            Index = index;
            Thumbstick = thumbstick;
            Position = position;
            Delta = delta;
        }
    }

    public struct GamePadTriggerEventArgs
    {
        public PlayerIndex Index { get; internal set; }
        public Trigger Trigger { get; internal set; }
        public Single Position { get; internal set; }
        public Single Delta { get; internal set; }

        public GamePadTriggerEventArgs(PlayerIndex index, Trigger trigger, Single position, Single delta)
        {
            Index = index;
            Trigger = trigger;
            Position = position;
            Delta = delta;
        }
    }
}
