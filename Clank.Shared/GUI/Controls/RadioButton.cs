﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clank.GUI.Controls
{
    public abstract class RadioButton : Button
    {
        private Boolean _selected;
        public Boolean Selected
        {
            get { return _selected; }
            set { if (_selected == value) return; _selected = value; SelectedChanged?.Invoke(this, _selected); }
        }
        public event GUISelfHandler<Boolean> SelectedChanged;

        public RadioButton(String groupName)
        {
            GroupName = groupName;
        }

        public String GroupName { get; private set; }

        protected internal override void DoMouseClick(Panel panel, MouseButtonEventArgs args)
        {
            if (!Selected)
            {
                foreach (var radio in panel.RadioGroups[GroupName].Where(radio => radio != this))
                {
                    radio.Selected = false;
                }
                Selected = true;
            }
            base.DoMouseClick(panel, args);
        }
    }
}
