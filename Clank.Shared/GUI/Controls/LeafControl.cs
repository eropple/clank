﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.GUI.Controls
{
    public abstract class LeafControl : Control
    {
        public sealed override void AddControl(Control control)
        {
            throw new GUIException("Cannot add children to LeafControl.");
        }

        public sealed override bool RemoveControl(Control control)
        {
            throw new GUIException("Cannot remove children from LeafControl.");
        }
    }
}
