﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Clank.Graphics;
using Clank.Input;
using Clank.Primitives;
using Clank.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Clank.GUI.Controls
{
    public delegate void GUIEventHandler<in TArgsType>(Panel panel, TArgsType args);

    public delegate void GUIDrawHandler<in TArgsType>(Panel panel, BatchStacker batch, TArgsType args);

    public delegate void GUISelfHandler<in TArgsType>(Control control, TArgsType args);
    public abstract class Control
    {
        protected static readonly Position DefaultPosition = new Position(0, 0);
        protected static readonly Matrix DefaultPositionMatrix = Matrix.Identity;

        private Position _position = DefaultPosition;
        private Matrix _positionMatrix = DefaultPositionMatrix;
        public Position Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _positionMatrix = Matrix.CreateTranslation(value.X, value.Y, 0);
            }
        }

        public Position RealPosition
        {
            get
            {
                var x = 0;
                var y = 0;
                var control = this;
                while (control != null)
                {
                    x += control.Position.X;
                    y += control.Position.Y;
                    control = control.Parent;
                }
                return new Position(x, y);
            }
        }

        public abstract Size Size { get; }
        public Rectangle Bounds => new Rectangle(Position.X, Position.Y, Size.Width, Size.Height);

        private Boolean _canFocus;
        public Boolean CanFocus
        {
            get {  return _canFocus; }
            set { _canFocus = value; Panel?.RegisterControl(this); }
        }

        public Int32 ZIndex { get; set; }

        public Int32 TabIndex { get; set; }

        public Control Parent { get; private set; }

        private Boolean _enabled;

        public Boolean Enabled
        {
            get { return _enabled && (Parent?.Enabled ?? true); }
            set { _enabled = value; }
        }

        public Panel Panel
        {
            get
            {
                var p = ControlChain.First() as Panel;
                return p != this ? p : null;
            }
        }

        private readonly List<Control> _childControls = new List<Control>();
        public IReadOnlyList<Control> ChildControls => _childControls;

        /// <summary>
        /// Any mouse input events that are invoked on child controls shall be invoked first
        /// on this control.
        /// </summary>
        public Boolean MousePreview { get; set; }
        /// <summary>
        /// If MousePreview is true and MouseSoak is true, mouse input events on child controls
        /// will be intercepted and not propagated further down the chain.
        /// </summary>
        public Boolean MouseSoak { get; set; }
        /// <summary>
        /// Any keyboard input events that are invoked on child controls shall be invoked first
        /// on this control.
        /// </summary>
        public Boolean KeyPreview { get; set; }
        /// <summary>
        /// If KewPreview is true and KewSoak is true, keyboard input events on child controls
        /// will be intercepted and not propagated further down the chain.
        /// </summary>
        public Boolean KeySoak { get; set; }

        // TODO: controllers
        public event GUIEventHandler<UpdateEventArgs> UpdateBeforeChildren;
        public event GUIEventHandler<UpdateEventArgs> UpdateAfterChildren;
        public event GUIDrawHandler<UpdateEventArgs> DrawBeforeChildren;  
        public event GUIDrawHandler<UpdateEventArgs> DrawAfterChildren;

        public event GUISelfHandler<ChildControlEventArgs> ChildAdded; 
        public event GUISelfHandler<ChildControlEventArgs> ChildRemoved;

        public event GUISelfHandler<FocusEventArgs> LostFocus;
        public event GUISelfHandler<FocusEventArgs> GotFocus;  

        public event GUIEventHandler<MouseButtonEventArgs> Click;
        public event GUIEventHandler<MouseButtonEventArgs> DoubleClick;
        public event GUIEventHandler<MouseButtonEventArgs> MouseDown;
        public event GUIEventHandler<MouseButtonEventArgs> MouseUp;
        public event GUIEventHandler<MouseWheelEventArgs> MouseWheel; 
        public event GUIEventHandler<MouseMoveEventArgs> MouseMove;
        public event GUIEventHandler<MouseMoveEventArgs> MouseEnter; 
        public event GUIEventHandler<MouseMoveEventArgs> MouseLeave;
        public event GUIEventHandler<KeyEventArgs> KeyPressed;
        public event GUIEventHandler<KeyEventArgs> KeyDown;
        public event GUIEventHandler<KeyEventArgs> KeyUp; 

        protected Control()
        {
            Enabled = true;
        }

        public IEnumerable<Control> ControlChain
        {
            get
            {
                var enumerable = this.Yield();
                var parent = Parent;
                while (parent != null)
                {
                    enumerable = parent.Yield().Concat(enumerable);
                    parent = parent.Parent;
                }
                return enumerable;
            }
        }

        public virtual void AddControl(Control control)
        {
            if (control.Parent != null)
            {
                throw new GUIException("Object added to ChildControls, but it already has a different parent.");
            }
            control.Parent = this;
            _childControls.Add(control);
            (Panel ?? this as Panel)?.RegisterControl(control);
            ChildAdded?.Invoke(this, new ChildControlEventArgs(control));
        }

        public virtual Boolean RemoveControl(Control control)
        {
            if (control.Parent != this)
            {
                throw new GUIException("Object removed from ChildControls, but it wasn't parented to us?");
            }
            control.Parent = null;
            var retval = _childControls.Remove(control);
            if (!retval) return false;

            ChildRemoved?.Invoke(this, new ChildControlEventArgs(control));
            (Panel ?? this as Panel)?.DeregisterControl(control);
            return true;
        }

        /// <summary>
        /// Given a position in its parent's space (i.e., any parent has subtracted out
        /// its own position first), discovers the highest Z-Index control that corresponds
        /// to that location. The entire control list can then be retrieved via ControlChain.
        /// </summary>
        public Control ResolveMousePoint(Point location)
        {
            if (!Bounds.Contains(location)) return null;

            var childLocation = new Point(location.X - Position.X, location.Y - Position.Y);
            return _childControls.Select(c => c.ResolveMousePoint(childLocation)).FirstOrDefault(c => c != null) ?? this;
        } 


        internal void DoUpdate(Panel panel, UpdateEventArgs args)
        {
            UpdateBeforeChildren?.Invoke(panel, args);
            foreach (var child in ChildControls)
            {
                child.DoUpdate(panel, args);
            }
            UpdateAfterChildren?.Invoke(panel, args);
        }

        internal void DoDraw(Panel panel, BatchStacker batch, UpdateEventArgs args)
        {
            batch.Push(_positionMatrix);
            DrawBeforeChildren?.Invoke(panel, batch, args);
            foreach (var child in ChildControls.OrderBy(c => c.ZIndex))
            {
                child.DoDraw(panel, batch, args);
            }
            DrawAfterChildren?.Invoke(panel, batch, args);
            batch.Pop();
        }

        protected internal virtual void DoKeyUp(Panel panel, KeyEventArgs args)
        {
            KeyUp?.Invoke(panel, args);
        }

        protected internal virtual void DoKeyDown(Panel panel, KeyEventArgs args)
        {
            KeyDown?.Invoke(panel, args);
        }

        protected internal virtual void DoMouseMove(Panel panel, MouseMoveEventArgs args)
        {
            MouseMove?.Invoke(panel, args);
        }
        protected internal virtual void DoMouseEnter(Panel panel, MouseMoveEventArgs args)
        {
            MouseEnter?.Invoke(panel, args);
        }
        protected internal virtual void DoMouseLeave(Panel panel, MouseMoveEventArgs args)
        {
            MouseMove?.Invoke(panel, args);
        }

        protected internal virtual void DoMouseWheel(Panel panel, MouseWheelEventArgs args)
        {
            MouseWheel?.Invoke(panel, args);
        }

        protected internal virtual void DoMouseUp(Panel panel, MouseButtonEventArgs args)
        {
            MouseUp?.Invoke(panel, args);
        }

        protected internal virtual void DoMouseDown(Panel panel, MouseButtonEventArgs args)
        {
            MouseDown?.Invoke(panel, args);
        }

        private Int64 _clickTime = 0L;
        protected internal virtual void DoMouseClick(Panel panel, MouseButtonEventArgs args)
        {
            var thisClickTime = DateTime.UtcNow.Ticks/TimeSpan.TicksPerMillisecond;
            Click?.Invoke(panel, args);

            if (thisClickTime - _clickTime < 350) DoubleClick?.Invoke(panel, args);
            _clickTime = thisClickTime;
        }

        protected internal virtual void DoLostFocus(Panel panel, FocusEventArgs args)
        {
            LostFocus?.Invoke(panel, args);
        }
        protected internal virtual void DoGotFocus(Panel panel, FocusEventArgs args)
        {
            GotFocus?.Invoke(panel, args);
        }

        public class ZOrderComparer : IComparer<Control>
        {
            public int Compare(Control x, Control y)
            {
                return x.ZIndex.CompareTo(y.ZIndex);
            }

            private ZOrderComparer() { }
            public static readonly ZOrderComparer Instance = new ZOrderComparer();
        }

        public class TabOrderComparer : IComparer<Control>
        {
            public int Compare(Control x, Control y)
            {
                return x.TabIndex.CompareTo(y.TabIndex);
            }

            private TabOrderComparer() {}
            public static readonly TabOrderComparer Instance = new TabOrderComparer();
        }
    }
}
