﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Clank.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Unglide;

namespace Clank.GUI.Controls
{
    /// <summary>
    /// A Panel is the root of a given GUI instance. It receives input events and
    /// dispatches them out to controls as appropriate.
    /// </summary>
    public abstract class Panel : Control
    {
        public event GUIEventHandler<FocusEventArgs> FocusChanged;

        public event GUIEventHandler<CancelableArgs<KeyEventArgs>> KeyUpIntercept;
        public event GUIEventHandler<CancelableArgs<KeyEventArgs>> KeyDownIntercept;

        private readonly List<Control> _focusList = new List<Control>();
        private readonly MultiValueDictionary<String, RadioButton> _radioGroups = new MultiValueDictionary<String, RadioButton>();
        public IReadOnlyDictionary<String, IReadOnlyCollection<RadioButton>> RadioGroups => _radioGroups;
        public Boolean DefaultTabBehavior { get; set; }

        public readonly Tweener Tweener = new Tweener();


        private Control _mouseDownTarget;
        public Control MouseDownTarget => _mouseDownTarget;
        private Control _mouseOverTarget;
        public Control MouseOverTarget => _mouseOverTarget;

        private Control _focusedControl;
        public Control FocusedControl
        {
            get {  return _focusedControl; }
            set
            {
                if (value == _focusedControl || !value.CanFocus) return;
                var focuses = new FocusEventArgs(_focusedControl, value);
                _focusedControl = focuses.FocusedControl;

                focuses.UnfocusedControl?.DoLostFocus(this, focuses);
                focuses.FocusedControl?.DoGotFocus(this, focuses);
                FocusChanged?.Invoke(this, focuses);
            }
        }

        protected Panel()
        {
            CanFocus = false;
            DefaultTabBehavior = true;
            UpdateBeforeChildren += (panel, args) => panel.Tweener.Update(args.Delta/1000.0f);
        }

        public Control PreviousFocusedControl
        {
            get
            {
                if (_focusList.Count == 0) return null;
                if (_focusedControl == null) return _focusList.LastOrDefault();

                return _focusList.First() == _focusedControl
                    ? _focusList.Last()
                    : _focusList.TakeWhile(c => c != _focusedControl).Last();
            }
        }
        public Control NextFocusedControl
        {
            get
            {
                if (_focusList.Count == 0) return null;
                if (_focusedControl == null) return _focusList.FirstOrDefault();

                return _focusList.Last() == _focusedControl
                    ? _focusList.First()
                    : _focusList.SkipWhile(c => c != _focusedControl).Skip(1).First();
            }
        }

        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchKeyUp(KeyEventArgs args)
        {
            var cancelableArgs = new CancelableArgs<KeyEventArgs>(args);
            KeyUpIntercept?.Invoke(this, cancelableArgs);
            if (cancelableArgs.Cancel) return true;

            var controlChain = _focusedControl?.ControlChain?.ToList();
            var thisControl = controlChain?.Last();
            if (thisControl == null) return false;

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                if ((control == thisControl || !control.KeyPreview) && control != thisControl) continue;

                control.DoKeyUp(this, args);
                if (control.KeyPreview) break;
            }

            return true;
        }
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchKeyDown(KeyEventArgs args)
        {
            var cancelableArgs = new CancelableArgs<KeyEventArgs>(args);
            KeyDownIntercept?.Invoke(this, cancelableArgs);
            if (cancelableArgs.Cancel) return true;

            if (DefaultTabBehavior && args.Key == Keys.Tab)
            {
                FocusedControl = args.Shift ? PreviousFocusedControl : NextFocusedControl;
                return true;
            }

            var controlChain = _focusedControl?.ControlChain?.ToList();
            var thisControl = controlChain?.Last();
            if (thisControl == null) return false;

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                if ((control == thisControl || !control.KeyPreview) && control != thisControl) continue;

                control.DoKeyDown(this, args);
                if (control.KeyPreview) break;
            }

            return true;
        }

        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchMouseMove(MouseMoveEventArgs args)
        {
            var controlChain = ResolveMousePoint(args.Position)?.ControlChain?.ToList();
            var thisControl = controlChain?.Last();
            if (_mouseOverTarget != thisControl)
            {
                _mouseOverTarget?.DoMouseLeave(this, args);
                thisControl?.DoMouseEnter(this, args);
                _mouseOverTarget = thisControl;
            }

            if (thisControl == null) return false;

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                args.Position = new Point(args.Position.X - Position.X, args.Position.Y - Position.X);
                if ((control == thisControl || !control.MousePreview) && control != thisControl) continue;

                control.DoMouseMove(this, args);
                if (control.MouseSoak || control == thisControl) break;
            }

            _mouseOverTarget = thisControl;
            return true;
        }
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchMouseWheel(MouseWheelEventArgs args)
        {
            var controlChain = ResolveMousePoint(args.Position)?.ControlChain?.ToList();
            if (controlChain == null)
            {
                _mouseDownTarget = null;
                return false;
            }
            var thisControl = controlChain.Last();

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                args.Position = new Point(args.Position.X - Position.X, args.Position.Y - Position.X);
                if ((control == thisControl || !control.MousePreview) && control != thisControl) continue;

                control.DoMouseWheel(this, args);
                if (control.MouseSoak || control == thisControl) return true;
            }

            return true;
        }
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchMouseUp(MouseButtonEventArgs args)
        {
            var controlChain = ResolveMousePoint(args.Position)?.ControlChain?.ToList();
            if (controlChain == null)
            {
                _mouseDownTarget = null;
                return false;
            }
            var thisControl = controlChain.Last();

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                args.Position = new Point(args.Position.X - Position.X, args.Position.Y - Position.X);
                if ((control == thisControl || !control.MousePreview) && control != thisControl) continue;

                control.DoMouseUp(this, args);
                if (control.MouseSoak || control == thisControl) break;
            }

            if (thisControl != _mouseDownTarget) return true;

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                args.Position = new Point(args.Position.X - Position.X, args.Position.Y - Position.X);
                if ((control == thisControl || !control.KeyPreview) && control != thisControl) continue;

                control.DoMouseClick(this, args);
                if (control.MouseSoak || control == thisControl) return true;
            }

            return true;
        }
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public Boolean DispatchMouseDown(MouseButtonEventArgs args)
        {
            var controlChain = ResolveMousePoint(args.Position)?.ControlChain?.ToList();
            if (controlChain == null)
            {
                _mouseDownTarget = null;
                return false;
            }
            var thisControl = _mouseDownTarget = controlChain.Last();

            foreach (var control in controlChain)
            {
                if (!control.Enabled) break;
                args.Position = new Point(args.Position.X - Position.X, args.Position.Y - Position.X);
                if ((control == thisControl || !control.KeyPreview) && control != thisControl) continue;

                control.DoMouseDown(this, args);
                if (control.MouseSoak || control == thisControl) return true;
            }

            return true;
        }

        internal void RegisterControl(Control control)
        {
            RegisterControl(control, true);
        }

        private void RegisterControl(Control control, Boolean sortList)
        {
            _focusList.Remove(control);
            if (control.CanFocus)
            {
                _focusList.Add(control);
            }
            var radio = control as RadioButton;
            if (radio != null)
            {
                _radioGroups.Add(radio.GroupName, radio);
            }
            foreach (var child in control.ChildControls) RegisterControl(child, false);
            if (sortList) _focusList.Sort(TabOrderComparer.Instance);
        }

        internal void DeregisterControl(Control control)
        {
            foreach (var child in control.ChildControls) DeregisterControl(child);
            _focusList.Remove(control);

            var radio = control as RadioButton;
            if (radio != null)
            {
                _radioGroups.Remove(radio.GroupName, radio);
            }
        }
    }
}
