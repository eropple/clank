﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Graphics.Fonts;
using Clank.Primitives;
using Microsoft.Xna.Framework;

namespace Clank.GUI.Controls
{
    // TODO: support vertical and horizontal alignment
    public class Label : LeafControl
    {
        protected BitmapFont _font;
        protected String _text;
        protected TextSource _textSource;
        protected Color _color;
        protected Size _padding;

        public override Size Size
        {
            get
            {
                var bounds = _textSource?.Bounds ?? new Rectangle();
                return new Size(bounds.Width, bounds.Height);
            }
        }

        public BitmapFont Font
        {
            get { return _font; }
            set { _font = value; ReflowText(); }
        }
        public String Text
        {
            get { return _text; }
            set { _text = value; ReflowText(); }
        }

        public Size Padding
        {
            get { return _padding; }
            set { _padding = value; ReflowText(); }
        }

        public Color Color
        {
            get { return _color; }
            set { if (_color == value) return; _color = value; ReflowText(); }
        }

        public Label(BitmapFont font)
        {
            _font = font;
            _color = Color.White;
            _padding = new Size();
            _text = String.Empty;

            DrawBeforeChildren += Label_DrawBeforeChildren;
        }

        private void Label_DrawBeforeChildren(Panel panel, Graphics.BatchStacker batch, UpdateEventArgs args)
        {
            _textSource?.Draw(batch, (Point)_padding);
        }

        protected virtual void ReflowText()
        {
            _textSource = Font.BuildTextSource(_text, _color, -1);
        }


    }

    public class WrappedLabel : Label
    {
        private Int32 _wrapWidth = 150;

        public Int32 WrapWidth
        {
            get { return _wrapWidth; }
            set { _wrapWidth = value; ReflowText(); }
        }

        public WrappedLabel(BitmapFont font) : base(font)
        {
        }

        protected override void ReflowText()
        {
            _textSource = Font.BuildTextSource(_text, _color, WrapWidth);
        }
    }

    public class HorizontalScrollingLabel : Label
    {
        public Int32 ViewportWidth { get; private set; }

        public HorizontalScrollingLabel(BitmapFont font, int viewportWidth) : base(font)
        {
            ViewportWidth = viewportWidth;
        }
    }
}
