﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Clank.GUI.Controls
{
    public abstract class Button : LeafControl
    {
        protected Button()
        {
            CanFocus = true;
        }

        protected internal override void DoKeyUp(Panel panel, KeyEventArgs args)
        {
            if (args.Key == Keys.Space || args.Key == Keys.Enter)
            {
                DoMouseClick(panel, new MouseButtonEventArgs(new Point(), MouseButton.Left, args.Shift, args.Control, args.Alt));
            }
            else
            {
                base.DoKeyUp(panel, args);
            }
        }
    }
}
