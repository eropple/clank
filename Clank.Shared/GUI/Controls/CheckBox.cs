﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clank.GUI.Controls
{
    public abstract class CheckBox : Button
    {
        private Boolean _checked;
        public Boolean Checked
        {
            get { return _checked; }
            set { if (_checked == value) return; _checked = value; CheckedChanged?.Invoke(this, _checked); }
        }
        public event GUISelfHandler<Boolean> CheckedChanged;

        protected internal override void DoMouseClick(Panel panel, MouseButtonEventArgs args)
        {
            Checked = !Checked;
            base.DoMouseClick(panel, args);
        }
    }
}
