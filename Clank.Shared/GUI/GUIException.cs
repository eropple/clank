﻿using System;
using System.Collections.Generic;
using System.Text;
using Clank.Utils;

namespace Clank.GUI
{
    public class GUIException : ClankException
    {
        public GUIException()
        {
        }

        public GUIException(string message) : base(message)
        {
        }

        public GUIException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public GUIException(Exception innerException, string message, params object[] args) : base(innerException, message, args)
        {
        }

        public GUIException(string message, params object[] args) : base(message, args)
        {
        }
    }
}
